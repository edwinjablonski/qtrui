CONFIG      += plugin release
TARGET      = ../../bin/qtrtooltip
TEMPLATE    = lib

HEADERS     = qtrtooltip.h \
              ../qtrDrawing/qtrdrawing.h \
              ../qtrCore/qtrcore.h
SOURCES     = qtrtooltip.cpp \
              ../qtrDrawing/qtrdrawing.cpp
RESOURCES   =  
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}
