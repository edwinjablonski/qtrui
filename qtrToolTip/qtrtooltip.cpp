/**************************************************************************
** qtrToolTip, tool tip for widgets
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrtooltip.h"

/*!
    \class qtrToolTip
    \brief Class qtrToolTip provides fully visualy customisable tool tip for qtrUi projects.
    \inmodule qtrUi

    qtrToolTip inherits from QWidget class and needs qtrDrawing for drawing whole body, text and, if needed, pixmap.

    If you want to add tool tip to, for example, qtrButton, you need to pass pointer to parent while creating qtrToolTip object:
    \code
        qtrUiMainWindow::qtrUiMainWindow(QWidget *parent) :
            QWidget(parent),
            ui(new Ui::qtrUiMainWindow)
        {
            ui->setupUi(this);
            myButton = new qtrButton;
            myToolTip = new qtrToolTip(myButton);
        }
    \endcode

    \warning To work properly qtrToolTip's parent widget have to call s_mouseLeave and s_mouseOver signals.
    \sa qtrDrawing, qtrCore, HelloWorld
*/

/*!
 * \brief Constructor
 *
 * Sets all variables to default, predefined values. You can change tool tip's properties after creating new object.
 *
 * Mouse tracking is set to \c true.
 *
 * The \a parent parameter is used to connect signals for leaving and hovering.
 */

qtrToolTip::qtrToolTip(QWidget *parent) :
    QWidget(parent),
    brush( QColor( 255, 255, 255, 255 ) ),
    pen_color( 0, 0, 0, 255 ),
    pen_width( 2 ),
    pen_style( Qt::SolidLine ),
    pen_cap_style( Qt::SquareCap ),
    pen_join_style( Qt::MiterJoin ),
    text( "qtrToolTip" ),
    font( "Arial" ),
    font_color( 0, 0, 0, 255 ),
    text_alignment( Qt::AlignCenter ),
    margins( 10 ),
    timer_interval( 1000 )
{
    this->setWindowFlags( Qt::ToolTip );
    this->setStyleSheet( "background:transparent;" );
    this->setAttribute( Qt::WA_TranslucentBackground, true );
    this->setMouseTracking( true );
    parent_widget = parent;

    layPlugin   = new QHBoxLayout;
    drwTip      = new qtrDrawing;

    timer = new QTimer(this);

    timer->setInterval( timer_interval );
    timer->setSingleShot( true );
    connect( timer, SIGNAL( timeout() ), this, SLOT( showMe() ) );
    connect( parent_widget, SIGNAL( s_mouseOver() ), this, SLOT( start() ) );
    connect( parent_widget, SIGNAL( s_mouseLeave() ), this, SLOT( kill() ) );

    QFontMetrics fontMetrics( font );
    drwTip->setFixedSize( fontMetrics.width( text ) + ( margins * 2 ), fontMetrics.height() + ( margins * 2 ) );
    drwTip->setBrush( brush );
    drwTip->setPenColor( pen_color );
    drwTip->setPenWidth( pen_width );
    drwTip->setText( text );
    drwTip->setFont( font );
    drwTip->setFontColor( font_color );

    layPlugin->setContentsMargins( margins, margins, margins, margins );
    layPlugin->addWidget( drwTip );
    this->setLayout( layPlugin );
}

/*!
 * \brief Constructor
 *
 * This constructor creates visually custom object. You need to pass font and BrushAndPen structures.
 *
 * Mouse tracking is set to \c true.
 *
 * \a parent parameter is used to connect signals for leaving and hovering..
 * \a in_font structure contain font properties.
 * \a in_face structure contain properties of brush and pen.
 * \a in_text this text will appear on button's face.
 */

qtrToolTip::qtrToolTip(QString in_text, qtr::Font *in_font, qtr::BrushAndPen *in_face, QWidget *parent):
    QWidget(parent),
    brush( in_face->brush ),
    pen_color( in_face->pen_color ),
    pen_width( in_face->pen_width ),
    pen_style( in_face->pen_style ),
    pen_cap_style( in_face->pen_cap_style ),
    pen_join_style( in_face->pen_join_style ),
    text( in_text ),
    font( in_font->font ),
    font_color( in_font->font_color ),
    text_alignment( Qt::AlignCenter ),
    margins( 10 ),
    timer_interval( 1000 )
{
    this->setWindowFlags( Qt::ToolTip );
    this->setStyleSheet( "background:transparent;" );
    this->setAttribute( Qt::WA_TranslucentBackground, true );
    this->setMouseTracking( true );
    parent_widget = parent;

    layPlugin   = new QHBoxLayout;
    drwTip      = new qtrDrawing;

    timer = new QTimer(this);

    timer->setInterval( timer_interval );
    timer->setSingleShot( true );
    connect( timer, SIGNAL( timeout() ), this, SLOT( showMe() ) );
    connect( parent_widget, SIGNAL( s_mouseOver() ), this, SLOT( start() ) );
    connect( parent_widget, SIGNAL( s_mouseLeave() ), this, SLOT( kill() ) );

    QFontMetrics fontMetrics( font );
    drwTip->setFixedSize( fontMetrics.width( text ) + ( margins * 2 ), fontMetrics.height() + ( margins * 2 ) );
    drwTip->setBrush( brush );
    drwTip->setPenColor( pen_color );
    drwTip->setPenWidth( pen_width );
    drwTip->setText( text );
    drwTip->setFont( font );
    drwTip->setFontColor( font_color );

    layPlugin->setContentsMargins( margins, margins, margins, margins );
    layPlugin->addWidget( drwTip );
    this->setLayout( layPlugin );
}
/*!
 * \brief Deconstructor
 */

qtrToolTip::~qtrToolTip()
{
    delete drwTip;
    delete layPlugin;
    delete timer;
}

void qtrToolTip::setBrush(QBrush in_brush)
{
    brush = in_brush;
    drwTip->setBrush( in_brush );
}

/*!
 * \property qtrToolTip::brush
 * \brief a QBrush qtrToolTip is using for drawing body. You can pass QColor.
 * \sa pen_color
 */

QBrush qtrToolTip::getBrush()
{
    return brush;
}

void qtrToolTip::setPenColor(QColor in_color)
{
    pen_color = in_color;
    drwTip->setPenColor( in_color );
}

/*!
 * \property qtrToolTip::pen_color
 * \brief a QColor of QPen. This value is used to define border color.
 * \sa pen_width
 */

QColor qtrToolTip::getPenColor()
{
    return pen_color;
}

void qtrToolTip::setPenWidth(qreal in_qreal)
{
    pen_width = in_qreal;
    drwTip->setPenWidth( in_qreal );
}

/*!
 * \property qtrToolTip::pen_width
 * \brief width of QPen used to draw border.
 * \sa pen_color
 * \note \c pen_width look better when even value is set.
 */

qreal qtrToolTip::getPenWidth()
{
    return pen_width;
}

void qtrToolTip::setPenStyle(Qt::PenStyle in_style)
{
    pen_style = in_style;
    drwTip->setPenStyle( in_style );
}

/*!
 * \property qtrToolTip::pen_style
 * \brief a QPen pen style.
 * \sa pen_width, pen_color, pen_cap_style, pen_join_style
 */

Qt::PenStyle qtrToolTip::getPenStyle()
{
    return pen_style;
}

void qtrToolTip::setPenCapStyle(Qt::PenCapStyle in_cap)
{
    pen_cap_style = in_cap;
    drwTip->setPenCapStyle( in_cap );
}

/*!
 * \property qtrToolTip::pen_cap_style
 * \brief a QPen cap style.
 * \sa pen_width, pen_color, pen_style, pen_join_style
 */

Qt::PenCapStyle qtrToolTip::getPenCapStyle()
{
    return pen_cap_style;
}

void qtrToolTip::setPenJoinStyle(Qt::PenJoinStyle in_join)
{
    pen_join_style = in_join;
    drwTip->setPenJoinStyle( in_join );
}

/*!
 * \property qtrToolTip::pen_join_style
 * \brief a QPen join style.
 * \sa pen_width, pen_color, pen_style, pen_cap_style
 */

Qt::PenJoinStyle qtrToolTip::getPenJoinStyle()
{
    return pen_join_style;
}

void qtrToolTip::setText(QString in_string)
{
    text = in_string;

    QFontMetrics fontMetrics( this->getFont() );
    drwTip->setFixedSize( fontMetrics.width( in_string ) + ( margins * 2 ), fontMetrics.height() + ( margins * 2 ) );
    drwTip->setText( in_string );
}

/*!
 * \property qtrToolTip::text
 * \brief string of text.
 * \note It is not possible to draw pixmap and text in the same time.
 * \sa font, font_color, text_alignment
 */

QString qtrToolTip::getText()
{
    return text;
}

void qtrToolTip::setFont(QFont in_font)
{
    font = in_font;

    QFontMetrics fontMetrics( in_font );
    drwTip->setFixedSize( fontMetrics.width( text ) + ( margins * 2 ), fontMetrics.height() + ( margins * 2 ) );
    drwTip->setFont( in_font );
}

/*!
 * \property qtrToolTip::font
 * \brief font.
 * \sa text, font_color, text_alignment
 */

QFont qtrToolTip::getFont()
{
    return font;
}

void qtrToolTip::setFontColor(QColor in_color)
{
    font_color = in_color;
    drwTip->setFontColor( in_color );
}

/*!
 * \property qtrToolTip::font_color
 * \brief text color.
 * \sa text, font, text_alignment
 */

QColor qtrToolTip::getFontColor()
{
    return font_color;
}

void qtrToolTip::setTextAlignment(Qt::Alignment in_alignment)
{
    text_alignment = in_alignment;
    drwTip->setTextAlignment( in_alignment );
}

/*!
 * \property qtrToolTip::text_alignment
 * \brief text alignment.
 * \sa text, font, font_color
 */

Qt::Alignment qtrToolTip::getTextAlignment()
{
    return text_alignment;
}

void qtrToolTip::setPix(QPixmap in_pixmap)
{
    pixmap = in_pixmap;

    drwTip->setFixedSize( in_pixmap.size().width(), in_pixmap.size().height() );
    drwTip->setPix( in_pixmap );
}

/*!
 * \property qtrToolTip::pixmap
 * \brief pixmap.
 * \note It is not possible to draw text and pixmap at the same time.
 * \note Given pixmap will be resized to qtrToolTip current size (and then resized with every resize event) without keeping proportions.
 */

QPixmap qtrToolTip::getPix()
{
    return pixmap;
}

void qtrToolTip::setMargins(int in_margins)
{
    margins = in_margins;
    layPlugin->setContentsMargins( in_margins, in_margins, in_margins, in_margins );

    if( pixmap.isNull() ){
        QFontMetrics fontMetrics( this->getFont() );
        drwTip->setFixedSize( fontMetrics.width( this->getText() ) + ( in_margins * 2 ), fontMetrics.height() + ( in_margins * 2 ) );
    }
}

/*!
 * \property qtrToolTip::margins
 * \brief margins of layPlugin.
 * \note QHBoxLayout needs four margins value but in this case every margin is equal.
 */

int qtrToolTip::getMargins()
{
    return margins;
}

void qtrToolTip::setInterval(int in_int)
{
    timer_interval = in_int;
    timer->setInterval( in_int );
}

/*!
 * \property qtrToolTip::timer_interval
 * \brief timer's interval.
 */

int qtrToolTip::getInterval()
{
    return timer_interval;
}

void qtrToolTip::start()
{
    if( !( this->getText().isEmpty() && this->getPix().isNull() ) ){
        timer->start( timer_interval );
        this->setGeometry( QCursor::pos().x() + 10, QCursor::pos().y() - 20, this->width(), this->height() );
    }
}

void qtrToolTip::showMe()
{
    show();
}

void qtrToolTip::kill()
{
    timer->stop();
    this->hide();
}
