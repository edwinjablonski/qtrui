/**************************************************************************
** qtrToolTip, tool tip for widgets
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#ifndef QTRTOOLTIP_H
#define QTRTOOLTIP_H

#include <QWidget>
#include <QHBoxLayout>
#include <QTimer>
#include <QMouseEvent>
#include <QFontMetrics>
#include "../../qtrDrawing/qtrdrawing.h"
#include "../../qtrCore/qtrcore.h"

class qtrToolTip : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QBrush           brush           WRITE setBrush          READ getBrush)

    Q_PROPERTY(QColor           pen_color       WRITE setPenColor       READ getPenColor)
    Q_PROPERTY(qreal            pen_width       WRITE setPenWidth       READ getPenWidth)
    Q_PROPERTY(Qt::PenStyle     pen_style       WRITE setPenStyle       READ getPenStyle)
    Q_PROPERTY(Qt::PenCapStyle  pen_cap_style   WRITE setPenCapStyle    READ getPenCapStyle)
    Q_PROPERTY(Qt::PenJoinStyle pen_join_style  WRITE setPenJoinStyle   READ getPenJoinStyle)

    Q_PROPERTY(QString          text            WRITE setText           READ getText)
    Q_PROPERTY(QFont            font            WRITE setFont           READ getFont)
    Q_PROPERTY(QColor           font_color      WRITE setFontColor      READ getFontColor)
    Q_PROPERTY(Qt::Alignment    text_alignment  WRITE setTextAlignment  READ getTextAlignment)
    Q_PROPERTY(QPixmap          pixmap          WRITE setPix            READ getPix)

    Q_PROPERTY(int              margins         WRITE setMargins        READ getMargins)
    Q_PROPERTY(int              timer_interval  WRITE setInterval       READ getInterval)

public:
    qtrToolTip(QWidget *parent = 0);
    qtrToolTip(QString in_text, qtr::Font *in_font, qtr::BrushAndPen *in_face, QWidget *parent = 0);
    ~qtrToolTip();

    void    setBrush(QBrush in_brush);
    QBrush  getBrush();

    void    setPenColor(QColor in_color);
    QColor  getPenColor();

    void    setPenWidth(qreal in_qreal);
    qreal   getPenWidth();

    void    setPenStyle(Qt::PenStyle in_style);
    Qt::PenStyle    getPenStyle();

    void    setPenCapStyle(Qt::PenCapStyle in_cap);
    Qt::PenCapStyle getPenCapStyle();

    void    setPenJoinStyle(Qt::PenJoinStyle in_join);
    Qt::PenJoinStyle    getPenJoinStyle();

    void    setText(QString in_string);
    QString getText();

    void    setFont(QFont in_font);
    QFont   getFont();

    void    setFontColor(QColor in_color);
    QColor  getFontColor();

    void    setTextAlignment(Qt::Alignment in_alignment);
    Qt::Alignment getTextAlignment();

    void    setPix(QPixmap in_pixmap);
    QPixmap getPix();

    void    setMargins(int in_margins);
    int     getMargins();

    void    setInterval(int in_int);
    int     getInterval();

    QHBoxLayout         *layPlugin;
    qtrDrawing          *drwTip;
    QTimer              *timer;

private slots:
    void    showMe();
    void    kill();
    void    start();

private:
    QBrush              brush;
    QColor              pen_color;
    qreal               pen_width;
    Qt::PenStyle        pen_style;
    Qt::PenCapStyle     pen_cap_style;
    Qt::PenJoinStyle    pen_join_style;

    QString             text;
    QFont               font;
    QColor              font_color;
    Qt::Alignment       text_alignment;
    QPixmap             pixmap;

    int                 margins;
    int                 timer_interval;

    QWidget             *parent_widget;
};

#endif
