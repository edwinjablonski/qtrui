#!/bin/bash
#WARNING: DON'T START THIS SCRIPT FROM OTHER DIRECTORY THEN ROOT DIRECTORY OF QTRUI PROJECT.
qtrui_path=${PWD}
second_argument="$2"

#Plugins
qtrdrawing="qtrDrawing"
qtrbutton="qtrButton"
qtrcheckbox="qtrCheckBox"
qtrwindowhandler="qtrWindowHandler"
qtrtooltip="qtrToolTip"

#Examples
ex_path="Examples"
ex_helloworld="HelloWorld"

function printHelp {
  printf "This is qtrUi building script.\n"
  printf "Options:\n\n"
  printf "help \t\t\t\t- show this message\n"
  printf "build_plugins <path_to_qmake>\t- build plugins\n"
  printf "build_examples <path_to_qmake>\t- build examples\n"
  printf "build_all <path_to_qmake>\t- build plugins and examples\n"
  printf "install <path_to_plugins> \t- copy plugins to given path\n"
  printf "clear_plugins \t\t\t- clear plugins build folders\n"
  printf "purge \t\t\t\t- removes bin and build directories and EVERYTHING what's inside of them\n\n" 
  printf "Example:\n"
  printf "./build.bash build_plugins /home/johndoe/Qt/5.3/gcc_64/bin/qmake\n"
  printf "If <path_to_qmake> contains spaces pass it in quotes.\n"
  printf "WARNING: DON'T START THIS SCRIPT FROM OTHER DIRECTORY THEN ROOT DIRECTORY OF QTRUI PROJECT.\n"
  exit
}

function buildThis {
  printf "\nBuild %s:\n" "$1"
  
  cd $qtrui_path 	#make sure we are in root path of qtrUi
  
  if [ "$2" != "" ]; then
    cd $2
  fi
  
  cd $1			#go to currently building class directory

  if [ ! -d "build" ]; then #if build directory do not exist make it
      mkdir -v build
  fi

  cd build 		#go to build dir
  pro_path=" ../$1.pro" #set .pro path of currently building class
  
  $second_argument -spec linux-g++ -o Makefile $pro_path
  make
  
  unset pro_path
}

function clearThis {
  printf "Clear %s.\n" "$1"
  
  cd $qtrui_path 	#make sure we are in root path of qtrUi
  cd $1			#go to plugin directory
  
  if [ ! -d "build" ]; then #if build directory do not exist 
    printf "$qtrui_path/$1/build directory does not exist. Skipping.\n"
  fi
  
  if [ -d "build" ]; then
    cd "build"
    printf "I'm in $qtrui_path/$1/build\n"
    rm -v ./*
  fi
}

function purgeThis {
  cd $qtrui_path 	#make sure we are in root path of qtrUi
  
  if [ "$2" != "" ]; then #if second path is passed
    cd $2/$1
    
    if [ ! -d "build" ]; then #if build directory do not exist 
      printf "$qtrui_path/$2/$1/build directory does not exist. Skipping.\n"
    fi
    
    if [ -d "build" ]; then
      printf "I'm in $qtrui_path/$2/$1\n"
      rm -v -R build
    fi 
    
    if [ -d "bin" ]; then
      rm -v -R bin
    fi 
  else #only one argument
    cd $1
    
    if [ ! -d "build" ]; then #if build directory do not exist 
      printf "$qtrui_path/$1/build directory does not exist. Skipping.\n"
    fi
    
    if [ -d "build" ]; then
      printf "I'm in $qtrui_path/$1\n"
      rm -v -R build
    fi  
  fi
}

function buildPlugins {
  if [ "$second_argument" == "" ]; then #no qmake path
    printf "I need full qmake path.\nExample:\n./build.bash build_plugins /home/johndoe/Qt/5.3/gcc_64/bin/qmake\n"
    exit
  fi

  buildThis $qtrdrawing
  buildThis $qtrbutton
  buildThis $qtrcheckbox
  buildThis $qtrwindowhandler
  buildThis $qtrtooltip  
  exit
}

function buildExamples {
  if [ "$second_argument" == "" ]; then #no qmake path
    printf "I need full qmake path.\nExample:\n./build.bash build_plugins /home/johndoe/Qt/5.3/gcc_64/bin/qmake\n"
    exit
  fi

  buildThis $ex_helloworld $ex_path
  exit
}

function buildAll {
  if [ "$second_argument" == "" ]; then #no qmake path
    printf "I need full qmake path.\nExample:\n./build.bash build_plugins /home/johndoe/Qt/5.3/gcc_64/bin/qmake\n"
    exit
  fi

  buildThis $qtrdrawing
  buildThis $qtrbutton
  buildThis $qtrcheckbox
  buildThis $qtrwindowhandler
  buildThis $qtrtooltip
  buildThis $ex_helloworld $ex_path
  exit
}

function installPlugins {
  if [ "$second_argument" == "" ]; then #no install path
    printf "I need plugins path.\nExample:\n./build.bash install /home/johndoe/Qt/Tools/QtCreator/bin/plugins/designer/\n"
    exit
  else
    cd $qtrui_path 	#make sure we are in root path of qtrUi
    
    if [ ! -d "bin" ]; then #if build directory do not exist 
      printf "$qtrui_path/bin directory does not exist. You need to start this script with build_plugins <path_to_qmake> arguments first.\n"
    fi
    
    if [ -d "bin" ]; then
      cd bin
      cp -v *plugin.so $second_argument #copy plugins only
    fi   
  fi
  
  exit
}

function clearPlugins {
  clearThis $qtrdrawing
  clearThis $qtrbutton
  clearThis $qtrcheckbox
  clearThis $qtrwindowhandler
  clearThis $qtrtooltip
  exit
}

function purgeEverything {
  printf "Are you sure you want to purge everything? [yes/no]:\n"
  read answer
  
  if [ "$answer" == "yes" ] || [ "$answer" == "y" ]; then
    cd $qtrui_path 	#make sure we are in root path of qtrUi
    
    if [ ! -d "bin" ]; then #if build directory do not exist 
      printf "$qtrui_path/bin directory does not exist. Skipping.\n"
    fi
    
    if [ -d "bin" ]; then
      printf "I'm in $qtrui_path\n"
      rm -v -R bin
    fi
    
    purgeThis $qtrdrawing
    purgeThis $qtrbutton
    purgeThis $qtrcheckbox
    purgeThis $qtrwindowhandler
    purgeThis $qtrtooltip
    purgeThis $ex_helloworld $ex_path
    exit
  else
    exit
  fi
}

if [ "$1" == "" ]; then #no argument passed - print help
  printHelp
fi

if [ "$1" == "help" ]; then #print help
  printHelp
fi

if [ "$1" == "build_plugins" ]; then #build plugins
  buildPlugins
fi

if [ "$1" == "build_examples" ]; then #build examples
  buildExamples
fi

if [ "$1" == "build_all" ]; then #build plugins and examples
  buildAll
fi

if [ "$1" == "install" ]; then
  installPlugins
fi

if [ "$1" == "clear_plugins" ]; then
  clearPlugins
fi

if [ "$1" == "purge" ]; then
  purgeEverything
fi

if [ "$1" != "help" ] || [ "$1" != "build_plugins" ] || [ "$1" != "build_examples" ] || [ "$1" != "build_all" ]|| [ "$1" != "clear_plugins" ] || [ "$1" != "purge" ] || [ "$1" != "install" ] || [ "$1" != "" ]; then
  printHelp
fi