/**************************************************************************
** qtrButton, simple window decoration extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#ifndef GFXBUTTON_H
#define GFXBUTTON_H

#include <QtUiPlugin/QDesignerCustomWidgetInterface>
#include <QWidget>
#include <QMouseEvent>
#include "../qtrCore/qtrcore.h"

class qtrButton : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString          text                    WRITE setText               READ getText)
    Q_PROPERTY(Qt::Alignment    text_alignment          WRITE setTextAlignment      READ getTextAlignment)
    Q_PROPERTY(QFont            font                    WRITE setFont               READ getFont)
    Q_PROPERTY(QColor           font_color              WRITE setFontColor          READ getFontColor)
    Q_PROPERTY(QColor           font_hover_color        WRITE setFontHoverColor     READ getFontHoverColor)

    Q_PROPERTY(QPixmap          pixmap                  WRITE setPix                READ getPix)
    Q_PROPERTY(QPixmap          pixmap_hover            WRITE setPixHover           READ getPixHover)

    Q_PROPERTY(QPixmap          icon                    WRITE setIcon               READ getIcon)
    Q_PROPERTY(QPixmap          icon_hover              WRITE setIconHover          READ getIconHover)
    Q_PROPERTY(int              margins                 WRITE setMargins            READ getMargins)

    Q_PROPERTY(QBrush           brush                   WRITE setBrush              READ getBrush)
    Q_PROPERTY(QColor           pen_color               WRITE setPenColor           READ getPenColor)
    Q_PROPERTY(qreal            pen_width               WRITE setPenWidth           READ getPenWidth)
    Q_PROPERTY(Qt::PenStyle     pen_style               WRITE setPenStyle           READ getPenStyle)
    Q_PROPERTY(Qt::PenCapStyle  pen_cap_style           WRITE setPenCapStyle        READ getPenCapStyle)
    Q_PROPERTY(Qt::PenJoinStyle pen_join_style          WRITE setPenJoinStyle       READ getPenJoinStyle)
    Q_PROPERTY(qreal            opacity                 WRITE setOpacity            READ getOpacity)

    Q_PROPERTY(QBrush           brush_hover             WRITE setBrushHover         READ getBrushHover)
    Q_PROPERTY(QColor           pen_hover_color         WRITE setPenHoverColor      READ getPenHoverColor)
    Q_PROPERTY(qreal            pen_hover_width         WRITE setPenHoverWidth      READ getPenHoverWidth)
    Q_PROPERTY(Qt::PenStyle     pen_hover_style         WRITE setPenHoverStyle      READ getPenHoverStyle)
    Q_PROPERTY(Qt::PenCapStyle  pen_hover_cap_style     WRITE setPenHoverCapStyle   READ getPenHoverCapStyle)
    Q_PROPERTY(Qt::PenJoinStyle pen_hover_join_style    WRITE setPenHoverJoinStyle  READ getPenHoverJoinStyle)
    Q_PROPERTY(qreal            opacity_hover           WRITE setOpacityHover       READ getOpacityHover)

public:
    qtrButton(QWidget *parent = 0);
    qtrButton(QString in_text, qtr::Font *in_font, qtr::BrushAndPen *in_normal, qtr::BrushAndPen *in_hover, QWidget *parent = 0);
    ~qtrButton();

    void    setText(QString in_text);
    QString getText();

    void    setTextAlignment(Qt::Alignment in_alignment);
    Qt::Alignment   getTextAlignment();

    void    setFont(QFont in_font);
    QFont   getFont();

    void    setFontColor(QColor in_color);
    QColor  getFontColor();

    void    setFontHoverColor(QColor in_color);
    QColor  getFontHoverColor();

    void    setPix(QPixmap in_pixmap);
    QPixmap getPix();

    void    setPixHover(QPixmap in_pixmap);
    QPixmap getPixHover();

    void    setIcon(QPixmap in_pixmap);
    QPixmap getIcon();

    void    setIconHover(QPixmap in_pixmap);
    QPixmap getIconHover();

    void    setMargins(int in_margins);
    int     getMargins();

    void    setBrush(QBrush in_color);
    QBrush  getBrush();

    void    setPenColor(QColor in_color);
    QColor  getPenColor();

    void    setPenStyle(Qt::PenStyle in_style);
    Qt::PenStyle    getPenStyle();

    void    setPenCapStyle(Qt::PenCapStyle in_cap);
    Qt::PenCapStyle getPenCapStyle();

    void    setPenJoinStyle(Qt::PenJoinStyle in_join);
    Qt::PenJoinStyle    getPenJoinStyle();

    void    setPenWidth(qreal in_size);
    qreal   getPenWidth();

    void    setOpacity(qreal in_opacity);
    qreal   getOpacity();

    void    setBrushHover(QBrush in_color);
    QBrush  getBrushHover();

    void    setPenHoverColor(QColor in_color);
    QColor  getPenHoverColor();

    void    setPenHoverStyle(Qt::PenStyle in_style);
    Qt::PenStyle    getPenHoverStyle();

    void    setPenHoverCapStyle(Qt::PenCapStyle in_cap);
    Qt::PenCapStyle getPenHoverCapStyle();

    void    setPenHoverJoinStyle(Qt::PenJoinStyle in_join);
    Qt::PenJoinStyle    getPenHoverJoinStyle();

    void    setPenHoverWidth(qreal in_size);
    qreal   getPenHoverWidth();

    void    setOpacityHover(qreal in_opacity);
    qreal   getOpacityHover();

    void    setFontStruct(qtr::Font *in_struct);
    qtr::Font   getFontStruct();

    void    setBnPStruct(qtr::BrushAndPen *in_struct);
    qtr::BrushAndPen    getBnPStruct();

    void    setBnPHoverStruct(qtr::BrushAndPen *in_struct);
    qtr::BrushAndPen    getBnPHoverStruct();

    void    setOpacityStruct(qtr::Opacity *in_struct);
    qtr::Opacity        getOpacityStruct();

    void    mousePressEvent(QMouseEvent *e);
    void    enterEvent(QEvent *e);
    void    leaveEvent(QEvent *e);

protected:
    void    paintEvent(QPaintEvent *e);

signals:
    void    s_mouseOver();
    void    s_mouseLeave();
    void    s_clicked();
    void    s_clickedRight();

private:
    QString             text;
    Qt::Alignment       text_alignment;
    QFont               font;
    QColor              font_color;
    QColor              font_hover_color;

    QPixmap             pixmap;
    QPixmap             pixmap_hover;

    QPixmap             icon;
    QPixmap             icon_hover;
    int                 margins;

    QBrush              brush;
    QColor              pen_color;
    qreal               pen_width;
    Qt::PenStyle        pen_style;
    Qt::PenCapStyle     pen_cap_style;
    Qt::PenJoinStyle    pen_join_style;
    qreal               opacity;

    QBrush              brush_hover;
    QColor              pen_hover_color;
    qreal               pen_hover_width;
    Qt::PenStyle        pen_hover_style;
    Qt::PenCapStyle     pen_hover_cap_style;
    Qt::PenJoinStyle    pen_hover_join_style;
    qreal               opacity_hover;

    bool                is_hover;

    qtr::Font           font_struct;
    qtr::BrushAndPen    bnp_struct;
    qtr::BrushAndPen    bnp_hover_struct;
    qtr::Opacity        opacity_struct;
};

#endif
