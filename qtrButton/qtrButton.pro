CONFIG      += plugin release
TARGET      = ../../bin/qtrbuttonplugin
TEMPLATE    = lib

HEADERS     =   qtrbutton.h \
                qtrbuttonplugin.h
SOURCES     =   qtrbutton.cpp \
                qtrbuttonplugin.cpp
RESOURCES   =   res/icons.qrc
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

