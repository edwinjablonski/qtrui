/**************************************************************************
** qtrButton, simple window decoration extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrbutton.h"

/*!
    \class qtrButton
    \inmodule qtrUi
    \brief Class qtrButton provides fully customizable push button widget for Qt projects.

    \image doc_qtrButton.jpg "qtrButton"

    qtrButton inherits form QWidget. Main purpose of this widget is to mimic push button but with fully visually customisable appear (thanks to QPainter).
    qtrButton got two basic "states" - normal and, when when mouse pointer is above qtrButton, hover state. Via properties you can change QBrush, QPen,
    text, font and even set pixmap for both, normal and hover, states.

    qtrButton emits signals for hovering, leaving widget's space, left or right mouse button click.

    For more info about QPainter visit Qt documentation.

    \sa qtrWindowHandler, qtrCore, HelloWorld
*/

/*!
 * \brief Constructor
 *
 * Sets all variables to default, predefined values. You can change button's properties after creating new object.
 *
 * Mouse tracking is set to \c true.
 *
 * The \a parent parameter is sent to the QWidget constructor.
 */

qtrButton::qtrButton( QWidget *parent ) :
    QWidget( parent ),
    text( "qtrButton" ),
    text_alignment( Qt::AlignCenter ),
    font( "Arial" ),
    font_color( 0, 0, 0, 255 ),
    font_hover_color( 0, 0, 0, 255 ),
    margins( 0 ),
    brush( QColor( 0, 0, 0, 0 ) ),
    pen_color( 0, 0, 0, 0 ),
    pen_width( 0 ),
    pen_style( Qt::SolidLine ),
    pen_cap_style( Qt::SquareCap ),
    pen_join_style( Qt::MiterJoin ),
    opacity( 1 ),
    brush_hover( QColor( 0, 0, 0, 0 ) ),
    pen_hover_color( 0, 0, 0, 0 ),
    pen_hover_width( 0 ),
    pen_hover_style( Qt::SolidLine ),
    pen_hover_cap_style( Qt::SquareCap ),
    pen_hover_join_style( Qt::MiterJoin ),
    opacity_hover( 1 ),
    is_hover( false )
{
    this->setMouseTracking( true );
    this->show();
}

/*!
 * \brief Constructor
 *
 * This constructor creates visually custom object. You need to pass font structure and two BrushAndPen structures - first one for normal state and second one for
 * hover state.
 *
 * Mouse tracking is set to \c true.
 *
 * \a parent parameter is sent to the QWidget constructor.
 * \a in_font structure contain font properties.
 * \a in_normal structure contain properties of brush and pen for normal state.
 * \a in_hover structure contain properties of brush and pen for hover state.
 * \a in_text this text will appear on button's face.
 */

qtrButton::qtrButton(QString in_text, qtr::Font *in_font, qtr::BrushAndPen *in_normal, qtr::BrushAndPen *in_hover, QWidget *parent) :
    QWidget( parent ),
    text( in_text ),
    text_alignment( Qt::AlignCenter ),
    font( in_font->font ),
    font_color( in_font->font_color ),
    font_hover_color( in_font->font_color ),
    margins( 0 ),
    brush( in_normal->brush ),
    pen_color( in_normal->pen_color ),
    pen_width( in_normal->pen_width ),
    pen_style( in_normal->pen_style ),
    pen_cap_style( in_normal->pen_cap_style ),
    pen_join_style( in_normal->pen_join_style ),
    opacity( 1 ),
    brush_hover( in_hover->brush ),
    pen_hover_color( in_hover->pen_color ),
    pen_hover_width( in_hover->pen_width ),
    pen_hover_style( in_hover->pen_style ),
    pen_hover_cap_style( in_hover->pen_cap_style ),
    pen_hover_join_style( in_hover->pen_join_style ),
    opacity_hover( 1 ),
    is_hover( false )
{
    this->setMouseTracking( true );
    this->show();
}

/*!
 * \brief Deconstructor
 *
 * Nothing. Literally.
 */

qtrButton::~qtrButton()
{
}

void qtrButton::setText(QString in_text)
{
    text = in_text;
    update();
}

/*!
 * \property qtrButton::text
 * \brief normal state text.
 * \sa font_color, text_alignment, font
 */

QString qtrButton::getText()
{
    return text;
}

void qtrButton::setTextAlignment(Qt::Alignment in_alignment)
{
    text_alignment = in_alignment;
    update();
}

/*!
 * \property qtrButton::text_alignment
 * \brief normal state text alignment.
 * \sa text, font, font_color
 */

Qt::Alignment qtrButton::getTextAlignment()
{
    return text_alignment;
}

void qtrButton::setFont(QFont in_font)
{
    font = in_font;
    update();
}

/*!
 * \property qtrButton::font
 * \brief a QFont type for normal state font.
 * \sa text, font_color, text_alignment
 */

QFont qtrButton::getFont()
{
    return font;
}

void qtrButton::setFontColor(QColor in_color)
{
    font_color = in_color;
    update();
}

/*!
 * \property qtrButton::font_color
 * \brief a QColor for normal state font color.
 * \sa text, font, text_alignment
 */

QColor qtrButton::getFontColor()
{
    return font_color;
}

void qtrButton::setFontHoverColor(QColor in_color)
{
    font_hover_color = in_color;
    update();
}

/*!
 * \property qtrButton::font_hover_color
 * \brief a QColor for hover state font color.
 * \sa text, text_alignment, font, font_color
 */

QColor qtrButton::getFontHoverColor()
{
    return font_hover_color;
}

void qtrButton::setPix(QPixmap in_pixmap)
{
    pixmap = in_pixmap;
    update();
}

/*!
 * \property qtrButton::pixmap
 * \brief a QPixmap for normal state. This pixmap will be resized to current qtrButton widget size and resize every time when resizeEvent occur.
 *
 * \note This function will hide text string from widget. If you want to set icon pixmap that will appear next to text use setIcon or setIconHover.
 *
 * \sa pixmap_hover, icon, icon_hover
 */

QPixmap qtrButton::getPix()
{
    return pixmap;
}

void qtrButton::setPixHover(QPixmap in_pixmap)
{
    pixmap_hover = in_pixmap;
    update();
}

/*!
 * \property qtrButton::pixmap_hover
 * \brief a QPixmap for hover state. This pixmap will be resized to current qtrButton widget size and resize every time when resizeEvent occur.
 *
 * \note This function will hide text string from widget. If you want to set icon pixmap that will appear next to text use setIcon or setIconHover.
 * .
 * \sa pixmap, icon, icon_hover
 */

QPixmap qtrButton::getPixHover()
{
    return pixmap_hover;
}

void qtrButton::setIcon(QPixmap in_pixmap)
{
    icon = in_pixmap;
    update();
}

/*!
 * \property qtrButton::icon
 * \brief a QPixmap for normal state icon. This icon will appear next to text. Setting pixmap via setPix or setPixHover is replacing text.
 * \sa icon_hover, pixmap, pixmap_hover
 */

QPixmap qtrButton::getIcon()
{
    return icon;
}

void qtrButton::setIconHover(QPixmap in_pixmap)
{
    icon_hover = in_pixmap;
    update();
}

/*!
 * \property qtrButton::icon_hover
 * \brief a QPixmap for hover state icon. This icon will appear next to text. Setting pixmap via setPix or setPixHover is replacing text.
 * \sa icon, pixmap, pixmap_hover
 */

QPixmap qtrButton::getIconHover()
{
    return icon_hover;
}

void qtrButton::setMargins(int in_margins)
{
    margins = in_margins;
}

/*!
 * \property qtrButton::margins
 * \brief margins int value.
 * \sa icon, pixmap, pixmap_hover
 */

int qtrButton::getMargins()
{
    return margins;
}

void qtrButton::setBrush(QBrush in_color)
{
    brush = in_color;
    update();
}

/*!
 * \property qtrButton::brush
 * \brief a QBrush for normal state. You can pass QColor.
 * \sa brush_hover, pen_color
 */

QBrush qtrButton::getBrush()
{
    return brush;
}

void qtrButton::setPenColor(QColor in_color)
{
    pen_color = in_color;
    update();
}

/*!
 * \property qtrButton::pen_color
 * \brief a QColor of QPen (border) for normal state.
 * \sa pen_width, pen_style, pen_cap_style, pen_join_style, pen_hover_color
 */

QColor qtrButton::getPenColor()
{
    return pen_color;
}

void qtrButton::setPenWidth(qreal in_size)
{
    pen_width = in_size;
    update();
}

/*!
 * \property qtrButton::pen_width
 * \brief width of QPen for normal state.
 * \sa pen_color, pen_style, pen_cap_style, pen_join_style, pen_hover_color
 */

qreal qtrButton::getPenWidth()
{
    return pen_width;
}

void qtrButton::setOpacity(qreal in_opacity)
{
    opacity = in_opacity;
    update();
}

/*!
 * \property qtrButton::opacity
 * \brief qreal value of opacity of button when on normal state.
 * \sa opacity_hover
 */

qreal qtrButton::getOpacity()
{
    return opacity;
}

void qtrButton::setPenStyle(Qt::PenStyle in_style)
{
    pen_style = in_style;
    update();
}

/*!
 * \property qtrButton::pen_style
 * \brief a QPen pen style for normal state.
 * \sa pen_color, pen_width, pen_cap_style, pen_join_style, pen_hover_color
 */

Qt::PenStyle qtrButton::getPenStyle()
{
    return pen_style;
}

void qtrButton::setPenCapStyle(Qt::PenCapStyle in_cap)
{
    pen_cap_style = in_cap;
    update();
}

/*!
 * \property qtrButton::pen_cap_style
 * \brief a QPen cap style for normal state.
 * \sa pen_color, pen_width, pen_style, pen_join_style, pen_hover_color
 */

Qt::PenCapStyle qtrButton::getPenCapStyle()
{
    return pen_cap_style;
}

void qtrButton::setPenJoinStyle(Qt::PenJoinStyle in_join)
{
    pen_join_style = in_join;
    update();
}

/*!
 * \property qtrButton::pen_join_style
 * \brief a QPen join style for normal state.
 * \sa pen_color, pen_width, pen_cap_style, pen_hover_color
 */

Qt::PenJoinStyle qtrButton::getPenJoinStyle()
{
    return pen_join_style;
}

void qtrButton::setBrushHover(QBrush in_color)
{
    brush_hover    = in_color;
    update();
}

/*!
 * \property qtrButton::brush_hover
 * \brief a QBrush for hover state. You can pass QColor.
 * \sa brush, pen_hover_color
 */

QBrush qtrButton::getBrushHover()
{
    return brush_hover;
}

void qtrButton::setPenHoverColor(QColor in_color)
{
    pen_hover_color = in_color;
    update();
}

/*!
 * \property qtrButton::pen_hover_color
 * \brief a QColor of QPen for hover state.
 * \sa pen_hover_width, pen_hover_style, pen_hover_cap_style, pen_hover_join_style, pen_color
 */

QColor qtrButton::getPenHoverColor()
{
    return pen_hover_color;
}

void qtrButton::setPenHoverWidth(qreal in_size)
{
    pen_hover_width = in_size;
    update();
}

/*!
 * \property qtrButton::pen_hover_width
 * \brief width of QPen for hover state.
 * \sa pen_hover_color, pen_hover_style, pen_hover_cap_style, pen_hover_join_style, pen_color
 */

qreal qtrButton::getPenHoverWidth()
{
    return pen_hover_width;
}

void qtrButton::setOpacityHover(qreal in_opacity)
{
    opacity_hover = in_opacity;
    update();
}

/*!
 * \property qtrButton::opacity_hover
 * \brief qreal value of opacity of button when on hover state.
 * \sa opacity
 */

qreal qtrButton::getOpacityHover()
{
    return opacity_hover;
}

void qtrButton::setFontStruct(qtr::Font *in_struct)
{
    font_struct.font        = in_struct->font;
    font_struct.font_color  = in_struct->font_color;

    this->setFont( in_struct->font );
    this->setFontColor( in_struct->font_color );
}

/*!
 * \property qtrButton::font_struct
 * \brief a font structure. This structure holds font properties.
 * \sa qtrCore
 */

qtr::Font qtrButton::getFontStruct()
{
    return font_struct;
}

void qtrButton::setBnPStruct(qtr::BrushAndPen *in_struct)
{
    bnp_struct.brush          = in_struct->brush;
    bnp_struct.pen_color      = in_struct->pen_color;
    bnp_struct.pen_width      = in_struct->pen_width;
    bnp_struct.pen_style      = in_struct->pen_style;
    bnp_struct.pen_cap_style  = in_struct->pen_cap_style;
    bnp_struct.pen_join_style = in_struct->pen_join_style;

    this->setBrush( in_struct->brush );
    this->setPenColor( in_struct->pen_color );
    this->setPenWidth( in_struct->pen_width );
    this->setPenStyle( in_struct->pen_style );
    this->setPenCapStyle( in_struct->pen_cap_style );
    this->setPenJoinStyle( in_struct->pen_join_style );
}

/*!
 * \property qtrButton::bnp_struct
 * \brief a brush and pen structure. This structure holds properties of brush and pen of normal state.
 * \sa qtrCore, bnp_hover_struct
 */

qtr::BrushAndPen qtrButton::getBnPStruct()
{
    return bnp_struct;
}

void qtrButton::setBnPHoverStruct(qtr::BrushAndPen *in_struct)
{
    bnp_hover_struct.brush          = in_struct->brush;
    bnp_hover_struct.pen_color      = in_struct->pen_color;
    bnp_hover_struct.pen_width      = in_struct->pen_width;
    bnp_hover_struct.pen_style      = in_struct->pen_style;
    bnp_hover_struct.pen_cap_style  = in_struct->pen_cap_style;
    bnp_hover_struct.pen_join_style = in_struct->pen_join_style;


    this->setBrushHover( in_struct->brush );
    this->setPenHoverColor( in_struct->pen_color );
    this->setPenHoverWidth( in_struct->pen_width );
    this->setPenHoverStyle( in_struct->pen_style );
    this->setPenHoverCapStyle( in_struct->pen_cap_style );
    this->setPenHoverJoinStyle( in_struct->pen_join_style );
}

/*!
 * \property qtrButton::bnp_hover_struct
 * \brief a brush and pen structure. This structure holds properties of brush and pen of hover state.
 * \sa qtrCore, bnp_struct
 */

qtr::BrushAndPen qtrButton::getBnPHoverStruct()
{
    return bnp_hover_struct;
}

void qtrButton::setOpacityStruct(qtr::Opacity *in_struct)
{
    opacity_struct.normal   = in_struct->normal;
    opacity_struct.hover    = in_struct->hover;
    update();
}

/*!
 * \property qtrButton::opacity_struct
 * \brief a opacity structure. This structure holds qreal values for opacity.
 * \sa opacity, opacity_hover, qtrCore
 */

qtr::Opacity qtrButton::getOpacityStruct()
{
    return opacity_struct;
}

void qtrButton::setPenHoverStyle(Qt::PenStyle in_style)
{
    pen_hover_style = in_style;
    update();
}

/*!
 * \property qtrButton::pen_hover_style
 * \brief a QPen pen style for hover state.
 * \sa pen_hover_color, pen_hover_width, pen_hover_cap_style, pen_hover_join_style, pen_color
 */

Qt::PenStyle qtrButton::getPenHoverStyle()
{
    return pen_hover_style;
}

void qtrButton::setPenHoverCapStyle(Qt::PenCapStyle in_cap)
{
    pen_hover_cap_style = in_cap;
    update();
}

/*!
 * \property qtrButton::pen_hover_cap_style
 * \brief a QPen cap style for hover state.
 * \sa pen_hover_color, pen_hover_width, pen_hover_style, pen_hover_join_style, pen_color
 */

Qt::PenCapStyle qtrButton::getPenHoverCapStyle()
{
    return pen_hover_cap_style;
}

void qtrButton::setPenHoverJoinStyle(Qt::PenJoinStyle in_join)
{
    pen_hover_join_style = in_join;
    update();
}

/*!
 * \property qtrButton::pen_hover_join_style
 * \brief a QPen join style for hover state.
 * \sa pen_hover_color, pen_hover_width, pen_hover_style, pen_hover_cap_style, pen_color
 */

Qt::PenJoinStyle qtrButton::getPenHoverJoinStyle()
{
    return pen_hover_join_style;
}

/*!
 * This event check if left or right mouse button was clicked on widget. If so signal will be emitted - s_clicked() for left mouse button or s_clickedRight()
 * for right mouse button.
 *
 * \a e is accepted.
 */

void qtrButton::mousePressEvent(QMouseEvent *e)
{
    if( e->button() == Qt::LeftButton ){
        emit s_clicked();
        e->accept();
    }

    if( e->button() == Qt::RightButton ){
        emit s_clickedRight();
        e->accept();
    }
}

/*!
    \fn void qtrButton::s_clicked()
    This signal is emited when left mouse button is clicked on widget.

    \sa s_clickedRight(), s_mouseOver(), s_mouseLeave(), mousePressEvent()
 */

/*!
    \fn void qtrButton::s_clickedRight()
    This signal is emited when right mouse button is clicked on widget.

    \sa s_clicked(), s_mouseOver(), s_mouseLeave(), mousePressEvent()
 */

/*!
 * This event check if mouse pointer enters widget's area. If so \c is_hover flag is set to \c true and s_mouseOver() signal emited.
 *
 * \a e is unused.
 * \sa leaveEvent()
 */

void qtrButton::enterEvent(QEvent *e)
{
    Q_UNUSED(e)

    is_hover = true;
    emit s_mouseOver();
    update();
}

/*!
    \fn void qtrButton::s_mouseOver()
    This signal is emited when mouse pointer hovers above widget's area.

    \sa s_clicked(), s_clickedRight(), s_mouseLeave(), enterEvent()
 */

/*!
    \fn void qtrButton::s_mouseLeave()
    This signal is emited when mouse pointer leaves above widget's area.

    \sa s_clicked(), s_clickedRight(), s_mouseOver(), leaveEvent()
 */

/*!
 * This event check if mouse pointer leavers widget's area. If so \c is_hover flag is set to \c false and s_mouseLeave() signal emited.
 *
 * \a e is unused.
 * \sa enterEvent()
 */

void qtrButton::leaveEvent(QEvent *e)
{
    Q_UNUSED(e)

    is_hover = false;
    emit s_mouseLeave();
    update();
}

/*!
 * Paint event creates QPainter object, sets brush, pen and then draw. qtrButton widget is set as a painting device.
 *
 * This event is updated every time when one of the properties is changed.
 *
 * \a e is unused.
 */

void qtrButton::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)

    QPainter painter( this );

    // mouse pointer is above widget
    if( is_hover ){
        painter.setOpacity( opacity_struct.hover );

        // icons are not set
        if( icon.isNull() && icon_hover.isNull() ){
            painter.setBrush( brush_hover );

            if( pen_hover_width == 0 )
                painter.setPen( Qt::NoPen );
            else
                painter.setPen( QPen( pen_hover_color, pen_hover_width, pen_hover_style, pen_hover_cap_style, pen_hover_join_style ) );

            painter.drawRect( this->rect() );

            // icons are not set, pixmaps are not set
            if( pixmap_hover.isNull() && pixmap.isNull() ){
                painter.setFont( font );
                painter.setPen( font_hover_color );
                painter.drawText( this->rect(), text_alignment, text );
            }

            //if pixmap_hover equals NULL then show normal state pixmap, icons are not set
            if( pixmap_hover.isNull() && !( pixmap.isNull() ) )
                painter.drawPixmap( this->rect(), pixmap, pixmap.rect() );
            else
                painter.drawPixmap( this->rect(), pixmap_hover, pixmap_hover.rect() );
        }

        // if hover icon is not set but normal state icon is
        if( icon_hover.isNull() && !( icon.isNull() ) ){
            painter.setBrush( brush_hover );

            if( pen_hover_width == 0 )
                painter.setPen( Qt::NoPen );
            else
                painter.setPen( QPen( pen_hover_color, pen_hover_width, pen_hover_style, pen_hover_cap_style, pen_hover_join_style ) );

            painter.drawRect( this->rect() );

            painter.drawPixmap( margins, margins, this->height() - margins * 2, this->height() - margins * 2, icon );

            painter.setFont( font );
            painter.setPen( font_hover_color );
            painter.drawText( this->size().height() + margins, margins, this->size().width() - this->size().height() - margins * 3,
                              this->size().height() - margins * 2, text_alignment, text );
        }

        // icon_hover is set
        if( !(icon_hover.isNull() ) ){
            painter.setBrush( brush_hover );

            if( pen_hover_width == 0 )
                painter.setPen( Qt::NoPen );
            else
                painter.setPen( QPen( pen_hover_color, pen_hover_width, pen_hover_style, pen_hover_cap_style, pen_hover_join_style ) );

            painter.drawRect( this->rect() );

            painter.drawPixmap( margins, margins, this->height() - margins * 2, this->height() - margins * 2, icon_hover );

            painter.setFont( font );
            painter.setPen( font_hover_color );
            painter.drawText( this->size().height() + margins, margins, this->size().width() - this->size().height() - margins * 3,
                              this->size().height() - margins * 2, text_alignment, text );

        }
    // normal state, is_hover = false
    } else {
        painter.setOpacity( opacity_struct.normal );

        //icons are not set
        if( ( icon.isNull() && icon_hover.isNull() ) || icon.isNull() ){
            painter.setBrush( brush );

            if( pen_width == 0 )
                painter.setPen( Qt::NoPen );
            else
                painter.setPen( QPen( pen_color, pen_width, pen_style, pen_cap_style, pen_join_style ) );

            painter.drawRect( this->rect() );

            if( pixmap.isNull() ){
                painter.setFont( font );
                painter.setPen( font_color );
                painter.drawText( this->rect(), text_alignment, text );
            } else {
                painter.drawPixmap( this->rect(), pixmap, pixmap.rect() );
            }
        }
        //icon is set
        if( !( icon.isNull() ) ){
            painter.setBrush( brush );

            if( pen_width == 0 )
                painter.setPen( Qt::NoPen );
            else
                painter.setPen( QPen( pen_color, pen_width, pen_style, pen_cap_style, pen_join_style ) );

            painter.drawRect( this->rect() );

            painter.drawPixmap( margins, margins, this->height() - margins * 2, this->height() - margins * 2, icon );

            painter.setFont( font );
            painter.setPen( font_color );
            painter.drawText( this->size().height() + margins, margins, this->size().width() - this->size().height() - margins * 3,
                              this->size().height() - margins * 2, text_alignment, text );

        }
    }
}
