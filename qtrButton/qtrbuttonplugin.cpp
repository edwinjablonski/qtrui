/**************************************************************************
** qtrButton, simple window decoration extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrbutton.h"
#include "qtrbuttonplugin.h"

#include <QtPlugin>

qtrButtonPlugin::qtrButtonPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void qtrButtonPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool qtrButtonPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *qtrButtonPlugin::createWidget(QWidget *parent)
{
    return new qtrButton(parent);
}

QString qtrButtonPlugin::name() const
{
    return QLatin1String("qtrButton");
}

QString qtrButtonPlugin::group() const
{
    return QLatin1String("qtrUi");
}

QIcon qtrButtonPlugin::icon() const
{
    return QIcon(QLatin1String(":/button_icon.png"));
}

QString qtrButtonPlugin::toolTip() const
{
    return QLatin1String("");
}

QString qtrButtonPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool qtrButtonPlugin::isContainer() const
{
    return false;
}

QString qtrButtonPlugin::domXml() const
{
    return QLatin1String("<widget class=\"qtrButton\" name=\"qtrButton\">\n</widget>\n");
}

QString qtrButtonPlugin::includeFile() const
{
    return QLatin1String("");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(gfxbuttonplugin, qtrButtonPlugin)
#endif // QT_VERSION < 0x050000
