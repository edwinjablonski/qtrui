#include "about.h"
#include "ui_about.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    //Move to center of the screen.
    this->move(QApplication::desktop()->screen()->rect().center() - this->rect().center());

    //Get rid of OS specific window border.
    this->setWindowFlags(Qt::FramelessWindowHint);

    //Create font structure for widgets. In this case we want to take OS default font.
    standard_font = new qtr::Font;
    standard_font->font = QGuiApplication::font();
    standard_font->font_color = QColor( 255, 255, 255, 255 );

    //Create Opacity structure for all buttons.
    button_opacity = new qtr::Opacity;
    button_opacity->normal           = 0.7;
    button_opacity->hover            = 1;

    //Create BrushAndPen structure for tool tip face.
    tip_face = new qtr::BrushAndPen;
    tip_face->brush                  = QBrush( QColor( 50, 50, 50, 255 ) );
    tip_face->pen_color              = QColor( 0, 0, 0, 255 );
    tip_face->pen_width              = 2;
    tip_face->pen_style              = Qt::SolidLine;
    tip_face->pen_cap_style          = Qt::SquareCap;
    tip_face->pen_join_style         = Qt::MiterJoin;

    //Change qtrDrawing caption font.
    ui->drwText->setFont( standard_font->font );

    //Create tool tip.
    tipClose    = new qtrToolTip( "Close", standard_font, tip_face, ui->winHandler->btnClose );

    //Set cursor and opacity values to qtrWindowHandler close button.
    ui->winHandler->btnClose->setCursor( Qt::PointingHandCursor );
    ui->winHandler->btnClose->setOpacityStruct( button_opacity );

    //Connect close to window slot.
    connect( ui->winHandler->btnClose, SIGNAL( s_clicked() ), this, SLOT( close() ) );
}

AboutDialog::~AboutDialog()
{
    delete standard_font;
    delete button_opacity;
    delete tip_face;

    delete ui;
}

void AboutDialog::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)

    QPainter painter( this );

    painter.setBrush( Qt::NoBrush );
    painter.setPen( Qt::NoPen );

    painter.drawPixmap( this->rect(), QPixmap(":/about.jpg"), QPixmap(":/about.jpg").rect() );
}
