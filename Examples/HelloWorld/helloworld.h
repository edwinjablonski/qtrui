/**************************************************************************
** HelloWorld qtrUi example app
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#ifndef HELLOWORLD_H
#define HELLOWORLD_H

#include <QWidget>
#include "../../qtrButton/qtrbutton.h"
#include "../../qtrWindowHandler/qtrwindowhandler.h"
#include "../../qtrToolTip/qtrtooltip.h"
#include "about.h"

namespace Ui {
class HelloWorld;
}

class HelloWorld : public QWidget
{
    Q_OBJECT

public:
    explicit HelloWorld(QWidget *parent = 0);
    ~HelloWorld();

    void paintEvent(QPaintEvent *e);

private:
    Ui::HelloWorld *ui;

    AboutDialog         *about;

    qtrDrawing          *drwBackground;
    qtrButton           *btnAbout;

    qtrToolTip          *tipAbout;
    qtrToolTip          *tipMin;
    qtrToolTip          *tipClose;

    qtr::Font           *standard_font;
    qtr::Font           *win_handler_font;

    qtr::BrushAndPen    *button_handler;
    qtr::BrushAndPen    *button_normal;
    qtr::BrushAndPen    *button_hover;
    qtr::BrushAndPen    *tip_face;

    qtr::Opacity        *button_opacity;
};

#endif // HELLOWORLD_H
