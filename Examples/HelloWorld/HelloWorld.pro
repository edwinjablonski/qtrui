#-------------------------------------------------
#
# Project created by QtCreator 2015-04-16T20:06:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ../bin/HelloWorld
TEMPLATE = app


SOURCES += main.cpp\
        helloworld.cpp \
        ../../qtrWindowHandler/qtrwindowhandler.cpp \
        ../../qtrButton/qtrbutton.cpp \
        ../../qtrDrawing/qtrdrawing.cpp \
        ../../qtrToolTip/qtrtooltip.cpp \
        ../../qtrSizingHandler/qtrsizinghandler.cpp \
        about.cpp \

HEADERS  += helloworld.h \
        ../../qtrWindowHandler/qtrwindowhandler.h \
        ../../qtrButton/qtrbutton.h \
        ../../qtrDrawing/qtrdrawing.h \
        ../../qtrToolTip/qtrtooltip.h \
        ../../qtrSizingHandler/qtrsizinghandler.h \
        about.h

FORMS    += \
        helloworld.ui \
        about.ui

RESOURCES += \
        res/res.qrc
