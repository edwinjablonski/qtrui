#ifndef ABOUT_H
#define ABOUT_H

#include <QWidget>
#include "../../qtrButton/qtrbutton.h"
#include "../../qtrWindowHandler/qtrwindowhandler.h"
#include "../../qtrToolTip/qtrtooltip.h"

namespace Ui {
class AboutDialog;
}

class AboutDialog : public QWidget
{
    Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent = 0);
    ~AboutDialog();

    void paintEvent(QPaintEvent *e);

private:
    Ui::AboutDialog *ui;

    qtrToolTip          *tipClose;

    qtr::Font           *standard_font;
    qtr::Opacity        *button_opacity;
    qtr::BrushAndPen    *tip_face;
};

#endif // ABOUT_H
