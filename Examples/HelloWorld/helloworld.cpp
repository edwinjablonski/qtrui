/**************************************************************************
** HelloWorld qtrUi example app
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "helloworld.h"
#include "ui_helloworld.h"

HelloWorld::HelloWorld(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HelloWorld)
{
    ui->setupUi(this);
    //Get rid of OS specific window border.
    this->setWindowFlags(Qt::FramelessWindowHint);

    //Create font structure for qtrWindowHandler caption. In this case we want to take OS default font.
    win_handler_font = new qtr::Font;
    win_handler_font->font = QGuiApplication::font();
    win_handler_font->font.setPointSize( 15 );
    win_handler_font->font_color = QColor( 255, 255, 255, 255 );

    //Create font structure for widgets. In this case we want to take OS default font.
    standard_font = new qtr::Font;
    standard_font->font = QGuiApplication::font();
    standard_font->font_color = QColor( 255, 255, 255, 255 );

    //Create BrushAndPen structure for additional qtrWindowHandler button.
    button_handler = new qtr::BrushAndPen;
    button_handler->brush            = QBrush( QColor( 0, 0, 0, 0 ) );
    button_handler->pen_color        = QColor( 0, 0, 0, 0 );
    button_handler->pen_width        = 0;
    button_handler->pen_style        = Qt::SolidLine;
    button_handler->pen_cap_style    = Qt::SquareCap;
    button_handler->pen_join_style   = Qt::MiterJoin;

    //Create BrushAndPen structure for button's normal state.
    button_normal = new qtr::BrushAndPen;
    button_normal->brush             = QBrush( QColor( 0, 0, 0, 0 ) );
    button_normal->pen_color         = QColor( 0, 0, 0, 0 );
    button_normal->pen_width         = 0;
    button_normal->pen_style         = Qt::SolidLine;
    button_normal->pen_cap_style     = Qt::SquareCap;
    button_normal->pen_join_style    = Qt::MiterJoin;

    //Create BrushAndPen structure for button's hover state.
    button_hover = new qtr::BrushAndPen;
    button_hover->brush              = QBrush( QColor( 0, 0, 0, 0 ) );
    button_hover->pen_color          = QColor( 0, 0, 0, 0 );
    button_hover->pen_width          = 0;
    button_hover->pen_style          = Qt::SolidLine;
    button_hover->pen_cap_style      = Qt::SquareCap;
    button_hover->pen_join_style     = Qt::MiterJoin;

    //Create BrushAndPen structure for tool tip face.
    tip_face = new qtr::BrushAndPen;
    tip_face->brush                  = QBrush( QColor( 50, 50, 50, 255 ) );
    tip_face->pen_color              = QColor( 0, 0, 0, 255 );
    tip_face->pen_width              = 2;
    tip_face->pen_style              = Qt::SolidLine;
    tip_face->pen_cap_style          = Qt::SquareCap;
    tip_face->pen_join_style         = Qt::MiterJoin;

    //Create Opacity structure for all buttons.
    button_opacity = new qtr::Opacity;
    button_opacity->normal           = 0.7;
    button_opacity->hover            = 1;

    //Create new qtrButton - it will be used for about button and will be added to qtrWindowHandler.
    btnAbout = new qtrButton( "", standard_font, button_handler, button_handler);
    btnAbout->setCursor( Qt::PointingHandCursor );
    btnAbout->setPix( QPixmap( ":/btn_about.png" ) );
    btnAbout->setOpacityStruct( button_opacity );

    //Create new form. In this case AboutDialog.
    about = new AboutDialog;
    about->hide();

    //Create tool tips.
    tipAbout    = new qtrToolTip( "About", standard_font, tip_face, btnAbout );
    tipMin      = new qtrToolTip( "Minimize", standard_font, tip_face, ui->winHandler->btnMin );
    tipClose    = new qtrToolTip( "Close", standard_font, tip_face, ui->winHandler->btnClose );

    //Add about button to qtrWindowHandler.
    ui->winHandler->insertWidget( 0, btnAbout );
    ui->winHandler->setFont( win_handler_font->font );

    //Set cursor and opacity values to qtrWindowHndler buttons.
    ui->winHandler->btnClose->setCursor( Qt::PointingHandCursor );
    ui->winHandler->btnClose->setOpacityStruct( button_opacity );
    ui->winHandler->btnMin->setCursor( Qt::PointingHandCursor );
    ui->winHandler->btnMin->setOpacityStruct( button_opacity );

    //Connect close and min buttons to application slots, so we can close and minimize app.
    connect( ui->winHandler->btnClose, SIGNAL( s_clicked() ), this, SLOT( close() ) );
    connect( ui->winHandler->btnMin, SIGNAL( s_clicked() ), this, SLOT( showMinimized() ) );
    connect( btnAbout, SIGNAL( s_clicked() ), about, SLOT( show() ) );
}

HelloWorld::~HelloWorld()
{
    delete tipAbout;
    delete tipMin;
    delete tipClose;
    delete btnAbout;

    delete standard_font;
    delete win_handler_font;
    delete button_handler;
    delete button_normal;
    delete button_hover;
    delete tip_face;

    delete about;
    delete ui;
}

void HelloWorld::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)

    QPainter painter( this );

    painter.setBrush( Qt::NoBrush );
    painter.setPen( Qt::NoPen );

    painter.drawPixmap( this->rect(), QPixmap(":/world.jpg"), QPixmap(":/world.jpg").rect() );
}
