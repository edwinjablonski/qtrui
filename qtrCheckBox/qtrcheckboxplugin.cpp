/**************************************************************************
** qtrCheckBox, simple two state checkbox extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrcheckbox.h"
#include "qtrcheckboxplugin.h"

#include <QtPlugin>

qtrCheckBoxPlugin::qtrCheckBoxPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void qtrCheckBoxPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool qtrCheckBoxPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *qtrCheckBoxPlugin::createWidget(QWidget *parent)
{
    return new qtrCheckBox(parent);
}

QString qtrCheckBoxPlugin::name() const
{
    return QLatin1String("qtrCheckBox");
}

QString qtrCheckBoxPlugin::group() const
{
    return QLatin1String("qtrUi");
}

QIcon qtrCheckBoxPlugin::icon() const
{
    return QIcon(QLatin1String(":/checkbox_icon.png"));
}

QString qtrCheckBoxPlugin::toolTip() const
{
    return QLatin1String("");
}

QString qtrCheckBoxPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool qtrCheckBoxPlugin::isContainer() const
{
    return false;
}

QString qtrCheckBoxPlugin::domXml() const
{
    return QLatin1String("<widget class=\"qtrCheckBox\" name=\"qtrCheckBox\">\n</widget>\n");
}

QString qtrCheckBoxPlugin::includeFile() const
{
    return QLatin1String("");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(qtrcheckboxplugin, qtrCheckBoxPlugin)
#endif // QT_VERSION < 0x050000
