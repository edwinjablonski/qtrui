/**************************************************************************
** qtrCheckBox, simple two state checkbox extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#ifndef QTRCHECKBOX_H
#define QTRCHECKBOX_H

#include <QtDesigner/QtDesigner>
#include <QWidget>
#include "../../qtrDrawing/qtrdrawing.h"
#include "../../qtrCore/qtrcore.h"

class qtrCheckBox : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QString          text                WRITE setText           READ getText)
    Q_PROPERTY(QFont            font                WRITE setFont           READ getFont)
    Q_PROPERTY(QColor           font_color          WRITE setFontColor      READ getFontColor)
    Q_PROPERTY(Qt::Alignment    text_alignment      WRITE setTextAlignment  READ getTextAlignment)

    Q_PROPERTY(int              margins             WRITE setLayoutMargins  READ getLayoutMargins)
    Q_PROPERTY(int              spacing             WRITE setLayoutSpacing  READ getLayoutSpacing)

    Q_PROPERTY(QPixmap          pixmap_unchecked    WRITE setPixUnchecked   READ getPixUnchecked)
    Q_PROPERTY(QPixmap          pixmap_checked      WRITE setPixChecked     READ getPixChecked)

    Q_PROPERTY(bool             is_checked          WRITE setChecked        READ isChecked)
    Q_PROPERTY(QCursor          cursor              WRITE setMouseCursor    READ getMouseCursor)

public:
    qtrCheckBox(QWidget *parent = 0);
    qtrCheckBox(QString in_text, qtr::Font *in_font, QWidget *parent = 0);
    ~qtrCheckBox();

    void    setText(QString in_text);
    QString getText();

    void    setFont(QFont in_font);
    QFont   getFont();

    void    setFontColor(QColor in_color);
    QColor  getFontColor();

    void    setTextAlignment(Qt::Alignment in_alignment);
    Qt::Alignment   getTextAlignment();

    void    setLayoutMargins(int in_margins);
    int     getLayoutMargins();

    void    setLayoutSpacing(int in_spacing);
    int     getLayoutSpacing();

    void    setChecked(bool in_state);
    bool    isChecked();

    void    setPixUnchecked(QPixmap in_pix);
    QPixmap getPixUnchecked();

    void    setPixChecked(QPixmap in_pix);
    QPixmap getPixChecked();

    void    setMouseCursor(QCursor in_cursor);
    QCursor getMouseCursor();

    void    mousePressEvent(QMouseEvent *e);
    void    enterEvent(QEvent *e);
    void    leaveEvent(QEvent *e);
    void    resizeEvent(QResizeEvent *e);

    QHBoxLayout     *layPlugin;
    qtrDrawing      *drwIcon;
    qtrDrawing      *drwText;

signals:
    void    s_mouseOver();
    void    s_mouseLeave();
    void    s_clicked();

private:
    QString         text;
    QFont           font;
    QColor          font_color;
    Qt::Alignment   text_alignment;

    int             margins;
    int             spacing;

    QPixmap         pixmap_unchecked;
    QPixmap         pixmap_checked;

    bool            is_checked;
    QCursor         cursor;
};

#endif
