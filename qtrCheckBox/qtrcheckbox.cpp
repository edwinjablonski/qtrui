/**************************************************************************
** qtrCheckBox, simple two state checkbox extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrcheckbox.h"

/*!
    \class qtrCheckBox
    \inmodule qtrUi
    \brief Class qtrCheckBox provides two state check box for Qt projects.

    \image doc_qtrCheckBox.jpg "qtrCheckBox"

    qtrCheckBox inherits form QWidget and uses qtrDrawing for drawing icon and text. This plugin shows two states that are visually represented by two boxes,
    for example - with ticked box and empty box, or with whatever graphical representation you like to.

    To change mouse cursor from Qt::PointingHandCursor to any other you need to call setMouseCursor from source code:

    \code
        qtrUiMainWindow::qtrUiMainWindow(QWidget *parent) :
            QWidget(parent),
            ui(new Ui::qtrUiMainWindow)
        {
            ui->setupUi(this);
            ui->myCheckBox->setMouseCursor( Qt::ArrowCursor );
        }
    \endcode

    \sa qtrDrawing
*/

/*!
 * \brief Constructor
 *
 * Sets all variables to default values.
 *
 * Mouse tracking is set to \c true.
 *
 * qtrDrawing drwText, qtrDrawing drwIcon and QHBoxLayout layPlugin are created and filled with default values.
 *
 * The \a parent parameter is sent to the QWidget constructor.
 */

qtrCheckBox::qtrCheckBox(QWidget *parent) :
    QWidget(parent),
    text( "qtrCheckBox" ),
    font( "Arial" ),
    font_color( 0, 0, 0, 255 ),
    text_alignment( Qt::AlignCenter ),
    margins( 0 ),
    spacing( 0 ),
    pixmap_unchecked( QPixmap( ":/unchecked.svg" ) ),
    pixmap_checked( QPixmap( ":/checked.svg" ) ),
    is_checked( false ),
    cursor( Qt::PointingHandCursor )
{
    this->setMouseTracking( true );

    drwText     = new qtrDrawing;
    drwIcon     = new qtrDrawing;
    layPlugin   = new QHBoxLayout;

    drwText->setMinimumSize( 0, 0 );
    drwIcon->setMaximumSize( this->size().height(), this->size().height() );
    drwIcon->setPix( pixmap_unchecked );
    drwIcon->setCursor( cursor );

    layPlugin->setContentsMargins( margins, margins, margins, margins );
    layPlugin->setSpacing( spacing );

    drwText->setText( text );

    layPlugin->addWidget( drwIcon );
    layPlugin->addWidget( drwText );
    this->setLayout( layPlugin );

    this->show();
}

/*!
 * \brief Constructor
 *
 * Sets all variables to default values.
 *
 * Mouse tracking is set to \c true.
 *
 * qtrDrawing drwText, qtrDrawing drwIcon and QHBoxLayout layPlugin are created and filled with default values.
 *
* \a parent parameter is sent to the QWidget constructor.
* \a in_font structure contain font properties.
* \a in_text this text will appear on button's face.
*
* \sa qtrCore
 */

qtrCheckBox::qtrCheckBox(QString in_text, qtr::Font *in_font, QWidget *parent):
    QWidget(parent),
    text( in_text ),
    font( in_font->font ),
    font_color( in_font->font_color ),
    text_alignment( Qt::AlignCenter ),
    margins( 0 ),
    spacing( 0 ),
    pixmap_unchecked( QPixmap( ":/unchecked.svg" ) ),
    pixmap_checked( QPixmap( ":/checked.svg" ) ),
    is_checked( false ),
    cursor( Qt::PointingHandCursor )
{
    this->setMouseTracking( true );

    drwText     = new qtrDrawing;
    drwIcon     = new qtrDrawing;
    layPlugin   = new QHBoxLayout;

    drwText->setMinimumSize( 0, 0 );
    drwIcon->setMaximumSize( this->size().height(), this->size().height() );
    drwIcon->setPix( pixmap_unchecked );
    drwIcon->setCursor( cursor );

    layPlugin->setContentsMargins( margins, margins, margins, margins );
    layPlugin->setSpacing( spacing );

    drwText->setText( text );

    layPlugin->addWidget( drwIcon );
    layPlugin->addWidget( drwText );
    this->setLayout( layPlugin );

    this->show();
}

/*!
 * \brief Deconstructor
 *
 * Deletes qtrDrawing drwIcon, qtrDrawing drwIcon and QHBoxLayout layPlugin.
 */

qtrCheckBox::~qtrCheckBox()
{
    delete drwText;
    delete drwIcon;
    delete layPlugin;
}

void qtrCheckBox::setText(QString in_text)
{
    text = in_text;
    drwText->setText( in_text );
    update();
}

/*!
 * \property qtrCheckBox::text
 * \brief text.
 * \sa font_color, text_alignment, font
 */

QString qtrCheckBox::getText()
{
    return text;
}

void qtrCheckBox::setFont(QFont in_font)
{
    font = in_font;
    drwText->setFont( in_font );
    update();
}

/*!
 * \property qtrCheckBox::font
 * \brief font.
 * \sa text, text_alignment, font_color
 */

QFont qtrCheckBox::getFont()
{
    return font;
}

void qtrCheckBox::setFontColor(QColor in_color)
{
    font_color = in_color;
    drwText->setFontColor( in_color );
    update();
}

/*!
 * \property qtrCheckBox::font_color
 * \brief font color.
 * \sa text, text_alignment, font
 */

QColor qtrCheckBox::getFontColor()
{
    return font_color;
}

void qtrCheckBox::setTextAlignment(Qt::Alignment in_alignment)
{
    text_alignment = in_alignment;
    drwText->setTextAlignment( in_alignment );
    update();
}

/*!
 * \property qtrCheckBox::text_alignment
 * \brief text alignment.
 * \sa text, font, font_color
 */

Qt::Alignment qtrCheckBox::getTextAlignment()
{
    return text_alignment;
}

void qtrCheckBox::setLayoutMargins(int in_margins)
{
    margins = in_margins;
    layPlugin->setContentsMargins( in_margins, in_margins, in_margins, in_margins );
    update();
}

/*!
 * \property qtrCheckBox::margins
 * \brief margins of QHBoxLayout layPlugin. QHBoxLayout needs four values for margins but in this case one value is used to set all of them.
 * \note For more infomration check out Qt documentation.
 * \sa spacing
 */

int qtrCheckBox::getLayoutMargins()
{
    return margins;
}

void qtrCheckBox::setLayoutSpacing(int in_spacing)
{
    spacing = in_spacing;
    layPlugin->setSpacing( in_spacing );
    update();
}

/*!
 * \property qtrCheckBox::spacing
 * \brief spacing of QHBoxLayout layPlugin.
 * \sa margins
 */

int qtrCheckBox::getLayoutSpacing()
{
    return spacing;
}

void qtrCheckBox::setChecked(bool in_state)
{
    is_checked = in_state;

    if( is_checked == 0 ){
        is_checked = 1;
        drwIcon->setPix( pixmap_checked );
        drwIcon->update();
    } else {
        is_checked = 0;
        drwIcon->setPix( pixmap_unchecked );
        drwIcon->update();
    }
}

/*!
 * \property qtrCheckBox::is_checked
 * \brief boolean flag for current state. \c flase means that check box is unchecked and \c true means that check box is checked.
 */

bool qtrCheckBox::isChecked()
{
    return is_checked;
}

void qtrCheckBox::setPixUnchecked(QPixmap in_pix)
{
    pixmap_unchecked = in_pix;
    update();
}

/*!
 * \property qtrCheckBox::pixmap_unchecked
 * \brief a QPixmap for unchecked state.
 * \sa pixmap_checked
 */

QPixmap qtrCheckBox::getPixUnchecked()
{
    return pixmap_unchecked;
}

void qtrCheckBox::setPixChecked(QPixmap in_pix)
{
    pixmap_checked = in_pix;
    update();
}

/*!
 * \property qtrCheckBox::pixmap_checked
 * \brief a QPixmap for checked state.
 * \sa pixmap_unchecked
 */

QPixmap qtrCheckBox::getPixChecked()
{
    return pixmap_checked;
}

void qtrCheckBox::setMouseCursor(QCursor in_cursor)
{
    cursor = in_cursor;
    drwIcon->setCursor( in_cursor );
}

/*!
 * \property qtrCheckBox::cursor
 * \brief a QCursor. This property defines drwIcon cursor only. You can't set this property for Qt Creator.
 */

QCursor qtrCheckBox::getMouseCursor()
{
    return cursor;
}

/*!
 * This event check if left mouse button was clicked on widget. If so signal will be emitted - s_clicked() and current state (via is_checked) changed.
 *
 * \a e is accepted when left mouse button is clicked.
 */

void qtrCheckBox::mousePressEvent(QMouseEvent *e)
{
    if( e->button() == Qt::LeftButton ){
        if( is_checked == 0 ){
            is_checked = 1;
            drwIcon->setPix( pixmap_checked );
        } else {
            is_checked = 0;
            drwIcon->setPix( pixmap_unchecked );
        }

        emit s_clicked();
        update();
        e->accept();
    }
}

/*!
    \fn void qtrCheckBox::s_clicked()
    This signal is emited when left mouse button is clicked on widget.

    \sa s_mouseOver(), s_mouseLeave(), mousePressEvent()
 */

/*!
 * This event check if mouse pointer enters widget's area. If so s_mouseOver() signal is emited.
 *
 * \a e is unused.
 * \sa leaveEvent()
 */

void qtrCheckBox::enterEvent(QEvent *e)
{
    Q_UNUSED(e)

    emit s_mouseOver();
}

/*!
 * This event check if mouse pointer leavers widget's area. If so s_mouseLeave() signal is emited.
 *
 * \a e is unused.
 * \sa enterEvent()
 */

void qtrCheckBox::leaveEvent(QEvent *e)
{
    Q_UNUSED(e)

    emit s_mouseLeave();
}

/*!
    \fn void qtrCheckBox::s_mouseOver()
    This signal is emited when mouse pointer hovers above widget's area.

    \sa s_clicked(), s_mouseLeave(), enterEvent()
 */

/*!
    \fn void qtrCheckBox::s_mouseLeave()
    This signal is emited when mouse pointer leaves above widget's area.

    \sa s_clicked(), s_mouseOver(), leaveEvent()
 */

/*!
 * This event resize drwIcon to keep proportion of pixmap. Now it's 1:1 proportion and it's not possible to change that.
 *
 * \a e is unused.
 */

void qtrCheckBox::resizeEvent(QResizeEvent *e)
{
    Q_UNUSED(e)

    drwIcon->setMaximumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
    drwIcon->setMinimumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
}
