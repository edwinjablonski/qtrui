CONFIG      += plugin release
TARGET      = ../../bin/qtrcheckboxplugin
TEMPLATE    = lib

HEADERS     = qtrcheckboxplugin.h \
              qtrcheckbox.h \
              ../qtrDrawing/qtrdrawing.h \
	      ../qtrCore/qtrcore.h
SOURCES     = qtrcheckboxplugin.cpp \
              qtrcheckbox.cpp \
              ../qtrDrawing/qtrdrawing.cpp
RESOURCES   = res/icons.qrc
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

