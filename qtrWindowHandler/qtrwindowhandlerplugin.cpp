/**************************************************************************
** qtrWindowHandler, simple window decoration extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrwindowhandler.h"
#include "qtrwindowhandlerplugin.h"

#include <QtPlugin>

qtrWindowHandlerPlugin::qtrWindowHandlerPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void qtrWindowHandlerPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool qtrWindowHandlerPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *qtrWindowHandlerPlugin::createWidget(QWidget *parent)
{
    return new qtrWindowHandler(parent);
}

QString qtrWindowHandlerPlugin::name() const
{
    return QLatin1String("qtrWindowHandler");
}

QString qtrWindowHandlerPlugin::group() const
{
    return QLatin1String("qtrUi");
}

QIcon qtrWindowHandlerPlugin::icon() const
{
    return QIcon(QLatin1String(":/windowhandler_icon.png"));
}

QString qtrWindowHandlerPlugin::toolTip() const
{
    return QLatin1String("");
}

QString qtrWindowHandlerPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool qtrWindowHandlerPlugin::isContainer() const
{
    return false;
}

QString qtrWindowHandlerPlugin::domXml() const
{
    return QLatin1String("<widget class=\"qtrWindowHandler\" name=\"qtrWindowHandler\">\n</widget>\n");
}

QString qtrWindowHandlerPlugin::includeFile() const
{
    return QLatin1String("");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(gfxwindowhandlerplugin, qtrWindowHandlerPlugin)
#endif // QT_VERSION < 0x050000
