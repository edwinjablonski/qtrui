CONFIG      += plugin release
TARGET      = ../../bin/qtrwindowhandlerplugin
TEMPLATE    = lib

HEADERS     = qtrwindowhandlerplugin.h \
              qtrwindowhandler.h \
              ../qtrButton/qtrbutton.h \
              ../qtrDrawing/qtrdrawing.h
SOURCES     = qtrwindowhandlerplugin.cpp \
              qtrwindowhandler.cpp \
              ../qtrButton/qtrbutton.cpp \
              ../qtrDrawing/qtrdrawing.cpp
RESOURCES   = res/icons.qrc
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}
