 /**************************************************************************
** qtrWindowHandler, simple window decoration extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#ifndef GFXWINDOWHANDLER_H
#define GFXWINDOWHANDLER_H

#include <QtUiPlugin/QDesignerCustomWidgetInterface>
#include <QWidget>
#include <QHBoxLayout>
#include <QMouseEvent>
#include <QApplication>
#include "../qtrButton/qtrbutton.h"
#include "../qtrDrawing/qtrdrawing.h"
#include "../qtrCore/qtrcore.h"

class qtrWindowHandler : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(QBrush           brush                       WRITE setBrush                      READ getBrush)
    Q_PROPERTY(QColor           pen_color                   WRITE setPenColor                   READ getPenColor)
    Q_PROPERTY(qreal            pen_width                   WRITE setPenWidth                   READ getPenWidth)
    Q_PROPERTY(Qt::PenStyle     pen_style                   WRITE setPenStyle                   READ getPenStyle)
    Q_PROPERTY(Qt::PenCapStyle  pen_cap_style               WRITE setPenCapStyle                READ getPenCapStyle)
    Q_PROPERTY(Qt::PenJoinStyle pen_join_style              WRITE setPenJoinStyle               READ getPenJoinStyle)

    Q_PROPERTY(bool             is_icon_visible             WRITE setIconVisible                READ isIconVisible)
    Q_PROPERTY(QPixmap          pixmap_icon                 WRITE setIconPix                    READ getIconPix)

    Q_PROPERTY(QString          text                        WRITE setText                       READ getText)
    Q_PROPERTY(QFont            font                        WRITE setFont                       READ getFont)
    Q_PROPERTY(QColor           font_color                  WRITE setFontColor                  READ getFontColor)
    Q_PROPERTY(Qt::Alignment    text_alignment              WRITE setTextAlignment              READ getTextAlignment)
    Q_PROPERTY(bool             is_text_in_center           WRITE setTextInCenter               READ isTextInCenter)

    Q_PROPERTY(int              margins                     WRITE setMargins                    READ getMargins)
    Q_PROPERTY(int              spacing_icon                WRITE setSpacingIcon                READ getSpacingIcon)
    Q_PROPERTY(int              spacing_buttons             WRITE setSpacingButtons             READ getSpacingButtons)

    Q_PROPERTY(bool             is_min_visible              WRITE setMinVisible                 READ isMinVisible)
    Q_PROPERTY(bool             is_max_visible              WRITE setMaxVisible                 READ isMaxVisible)

    Q_PROPERTY(QPixmap          pixmap_min                  WRITE setMinPix                     READ getMinPix)
    Q_PROPERTY(QPixmap          pixmap_max                  WRITE setMaxPix                     READ getMaxPix)
    Q_PROPERTY(QPixmap          pixmap_close                WRITE setClosePix                   READ getClosePix)

    Q_PROPERTY(QBrush           brush_buttons               WRITE setBrushButtons               READ getBrushButtons)
    Q_PROPERTY(QColor           pen_buttons_color           WRITE setPenButtonsColor            READ getPenButtonsColor)
    Q_PROPERTY(qreal            pen_buttons_width           WRITE setPenButtonsWidth            READ getPenButtonsWidth)
    Q_PROPERTY(Qt::PenStyle     pen_buttons_style           WRITE setPenButtonsStyle            READ getPenButtonsStyle)
    Q_PROPERTY(Qt::PenCapStyle  pen_buttons_cap_style       WRITE setPenButtonsCapStyle         READ getPenButtonsCapStyle)
    Q_PROPERTY(Qt::PenJoinStyle pen_buttons_join_style      WRITE setPenButtonsJoinStyle        READ getPenButtonsJoinStyle)

public:
    qtrWindowHandler(QWidget *parent = 0);
    qtrWindowHandler(QString in_text, qtr::Font *in_font, qtr::BrushAndPen *in_bg, qtr::BrushAndPen *in_btn, QWidget *parent = 0);
    ~qtrWindowHandler();

    void    setBrush(QBrush in_brush);
    QBrush  getBrush();

    void    setPenColor(QColor in_color);
    QColor  getPenColor();

    void    setPenWidth(qreal in_qreal);
    qreal   getPenWidth();

    void    setPenStyle(Qt::PenStyle in_style);
    Qt::PenStyle    getPenStyle();

    void    setPenCapStyle(Qt::PenCapStyle in_cap);
    Qt::PenCapStyle getPenCapStyle();

    void    setPenJoinStyle(Qt::PenJoinStyle in_join);
    Qt::PenJoinStyle    getPenJoinStyle();

    void    setIconVisible(bool in_visible);
    bool    isIconVisible();

    void    setIconPix(QPixmap in_pix);
    QPixmap getIconPix();

    void    setText(QString in_string);
    QString getText();

    void    setFont(QFont in_font);
    QFont   getFont();

    void    setFontColor(QColor in_color);
    QColor  getFontColor();

    void    setTextAlignment(Qt::Alignment in_alignment);
    Qt::Alignment getTextAlignment();

    void    setTextInCenter(bool in_flag);
    bool    isTextInCenter();

    void    setMargins(int in_margins);
    int     getMargins();

    void    setSpacingIcon(int in_spacing);
    int     getSpacingIcon();

    void    setSpacingButtons(int in_spacing);
    int     getSpacingButtons();

    void    setMinVisible(bool in_visible);
    bool    isMinVisible();

    void    setMaxVisible(bool in_visible);
    bool    isMaxVisible();

    void    setMinPix(QPixmap in_pix);
    QPixmap getMinPix();

    void    setMaxPix(QPixmap in_pix);
    QPixmap getMaxPix();

    void    setClosePix(QPixmap in_pix);
    QPixmap getClosePix();

    void    setBrushButtons(QBrush in_brush);
    QBrush  getBrushButtons();

    void    setPenButtonsColor(QColor in_color);
    QColor  getPenButtonsColor();

    void    setPenButtonsWidth(qreal in_qreal);
    qreal   getPenButtonsWidth();

    void    setPenButtonsStyle(Qt::PenStyle in_style);
    Qt::PenStyle    getPenButtonsStyle();

    void    setPenButtonsCapStyle(Qt::PenCapStyle in_cap);
    Qt::PenCapStyle getPenButtonsCapStyle();

    void    setPenButtonsJoinStyle(Qt::PenJoinStyle in_join);
    Qt::PenJoinStyle    getPenButtonsJoinStyle();

    void    insertWidget(int in_index, QWidget *in_widget);

    void    mouseMoveEvent(QMouseEvent *e);
    void    mousePressEvent(QMouseEvent *e);
    void    mouseReleaseEvent(QMouseEvent *e);
    void    enterEvent(QEvent *e);
    void    leaveEvent(QEvent *e);
    void    resizeEvent(QResizeEvent *e);

    QHBoxLayout *layPlugin;
    QHBoxLayout *layIconAndCaption;
    QHBoxLayout *layButtons;

    qtrDrawing  *drwIcon;
    qtrDrawing  *drwText;

    qtrButton   *btnMin;
    qtrButton   *btnMax;
    qtrButton   *btnClose;

protected:
    void    paintEvent(QPaintEvent *e);

signals:
    void    s_iconClickedLeft();
    void    s_iconClickedRight();
    void    s_draging();
    void    s_mouseOver();
    void    s_mouseLeave();

private:
    QBrush              brush;
    QColor              pen_color;
    qreal               pen_width;
    Qt::PenStyle        pen_style;
    Qt::PenCapStyle     pen_cap_style;
    Qt::PenJoinStyle    pen_join_style;

    bool                is_icon_visible;
    QPixmap             pixmap_icon;

    QString             text;
    QFont               font;
    QColor              font_color;
    Qt::Alignment       text_alignment;
    bool                is_text_in_center;

    int                 margins;
    int                 spacing_icon;
    int                 spacing_buttons;

    bool                is_min_visible;
    bool                is_max_visible;

    QPixmap             pixmap_min;
    QPixmap             pixmap_max;
    QPixmap             pixmap_close;

    QBrush              brush_buttons;
    QColor              pen_buttons_color;
    qreal               pen_buttons_width;
    Qt::PenStyle        pen_buttons_style;
    Qt::PenCapStyle     pen_buttons_cap_style;
    Qt::PenJoinStyle    pen_buttons_join_style;

    QPoint              drag_pos;
    bool                is_lmb_pressed;
    bool                is_cursor_overwritten;
};

#endif
