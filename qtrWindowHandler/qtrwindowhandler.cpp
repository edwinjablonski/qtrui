/**************************************************************************
** qtrWindowHandler, simple window decoration extansion, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrwindowhandler.h"

/*!
    \class qtrWindowHandler
    \inmodule qtrUi
    \brief Class qtrWindowHandler provides customisable window decoration and handle bar (this one with icon, window caption and buttons such minimize,
           maximize and close).

    \image doc_qtrWindowHandler.jpg "qtrWindowHandler"

    qtrWindowHandler mimics normal window handle bar with dragging ability and set of prepared buttons. It inherits from QWidget and is using qtrDrawing
    and qtrButton. Every widget is put inside of QHBoxLayout. Whole body is painted by QPainter.

    You can disable btnMin and btnMax by calling setMinVisible() and setMaxVisible().

    For more customizable options you can call one of qtrButton or qtrDrawing directly, for example:

    \code
        qtrUiMainWindow::qtrUiMainWindow(QWidget *parent) :
            QWidget(parent),
            ui(new Ui::qtrUiMainWindow)
        {
            ui->setupUi(this);
            ui->myWindowHandler->btnMax->setBrush( QColor( 255, 0, 0, 100 ) );
        }
    \endcode

    \note You need to manually disable window borders in Qt Creator or via source code. For more info visit Qt documentation (windowFlags).
    \note For now qtrWindowHandler works only with QWidget so it is not possible to use it with QMainWindow.
    \warning On some platforms disabling window border is not possible or some behaviors (like, for example, window shadows) will not occur.
    \sa qtrDrawing, qtrButton, qtrCore, HelloWorld
*/

/*!
 * \brief Constructor
 *
 * Sets all variables to default values.
 *
 * Mouse tracking is set to \c true.
 *
 * qtrDrawing drwText, drwIcon, qtrButton btnMin, btnMax, btnClose and QHBoxLayout layPlugin, layIconAndCaption, layButtons are
 * created and filled with default values.
 *
 * The \a parent parameter is sent to the QWidget constructor.
 */

qtrWindowHandler::qtrWindowHandler(QWidget *parent) :
    QWidget(parent),
    brush( QColor( 255, 255, 255, 255 ) ),
    pen_color( 0, 0, 0, 0 ),
    pen_width( 0 ),
    pen_style( Qt::SolidLine ),
    pen_cap_style( Qt::SquareCap ),
    pen_join_style( Qt::MiterJoin ),
    is_icon_visible( true ),
    pixmap_icon( QPixmap( ":/windowhandler_icon.png" ) ),
    text( "qtrDrawing" ),
    font( "Arial" ),
    font_color( 0, 0, 0, 255 ),
    text_alignment( Qt::AlignCenter ),
    is_text_in_center( false ),
    margins( 0 ),
    spacing_icon( 0 ),
    spacing_buttons( 0 ),
    is_min_visible( true ),
    is_max_visible( true ),
    pixmap_min( QPixmap( ":/min.svg" ) ),
    pixmap_max( QPixmap( ":/max.svg" ) ),
    pixmap_close( QPixmap( ":/close.svg" ) ),
    brush_buttons( QColor( 255, 255, 255, 255 ) ),
    pen_buttons_color( 0, 0, 0, 0 ),
    pen_buttons_width( 0 ),
    pen_buttons_style( Qt::SolidLine ),
    pen_buttons_cap_style( Qt::SquareCap ),
    pen_buttons_join_style( Qt::MiterJoin ),
    is_lmb_pressed( false ),
    is_cursor_overwritten( false )
{
    this->setMouseTracking( true );

    layPlugin           = new QHBoxLayout;
    layIconAndCaption   = new QHBoxLayout;
    layButtons          = new QHBoxLayout;

    drwIcon             = new qtrDrawing;
    drwText             = new qtrDrawing;

    btnMin              = new qtrButton;
    btnMax              = new qtrButton;
    btnClose            = new qtrButton;

    drwIcon->setPix( pixmap_icon );

    drwText->setText( text );

    btnMin->setPix( pixmap_min );
    btnMax->setPix( pixmap_max );
    btnClose->setPix( pixmap_close );

    btnMin->setBrush( brush_buttons );
    btnMin->setPenColor( pen_buttons_color );
    btnMin->setPenStyle( pen_buttons_style );
    btnMin->setPenCapStyle( pen_buttons_cap_style );
    btnMin->setPenJoinStyle( pen_buttons_join_style );

    btnMax->setBrush( brush_buttons );
    btnMax->setPenColor( pen_buttons_color );
    btnMax->setPenStyle( pen_buttons_style );
    btnMax->setPenCapStyle( pen_buttons_cap_style );
    btnMax->setPenJoinStyle( pen_buttons_join_style );

    btnClose->setBrush( brush_buttons );
    btnClose->setPenColor( pen_buttons_color );
    btnClose->setPenStyle( pen_buttons_style );
    btnClose->setPenCapStyle( pen_buttons_cap_style );
    btnClose->setPenJoinStyle( pen_buttons_join_style );

    layPlugin->setContentsMargins( margins, margins, margins, margins );
    layPlugin->setSpacing( 0 );

    layIconAndCaption->setContentsMargins( 0, 0, 0, 0 );
    layIconAndCaption->setSpacing( spacing_icon );

    layButtons->setContentsMargins( 0, 0, 0, 0 );
    layButtons->setSpacing( spacing_buttons );

    layIconAndCaption->addWidget( drwIcon );
    layIconAndCaption->addWidget( drwText );

    layButtons->addWidget( btnMin );
    layButtons->addWidget( btnMax );
    layButtons->addWidget( btnClose );

    layPlugin->addLayout( layIconAndCaption );
    layPlugin->addLayout( layButtons );

    drwIcon->setMaximumSize( this->size().height(), this->size().height() );
    drwIcon->setMinimumSize( this->size().height(), this->size().height() );

    btnMin->setMaximumSize( this->size().height(), this->size().height() );
    btnMin->setMinimumSize( this->size().height(), this->size().height() );

    btnMax->setMaximumSize( this->size().height(), this->size().height() );
    btnMax->setMinimumSize( this->size().height(), this->size().height() );

    btnClose->setMaximumSize( this->size().height(), this->size().height() );
    btnClose->setMinimumSize( this->size().height(), this->size().height() );

    this->setLayout( layPlugin );
    this->show();
}

/*!
 * \brief Constructor
 *
 * Sets all variables to default values.
 *
 * Mouse tracking is set to \c true.
 *
 * qtrDrawing drwText, drwIcon, qtrButton btnMin, btnMax, btnClose and QHBoxLayout layPlugin, layIconAndCaption, layButtons are
 * created and filled with default values.
 *
 * \a parent parameter is sent to the QWidget constructor.
 * \a in_font structure contain font properties.
 * \a in_bg structure contain properties of brush and pen for handler body.
 * \a in_btn structure contain properties of brush and pen for buttons.
 * \a in_text this text will appear on button's face.
 *
 * \sa qtrCore
 */

qtrWindowHandler::qtrWindowHandler(QString in_text, qtr::Font *in_font, qtr::BrushAndPen *in_bg, qtr::BrushAndPen *in_btn, QWidget *parent) :
    QWidget(parent),
    brush( in_bg->brush ),
    pen_color( in_bg->pen_color ),
    pen_width( in_bg->pen_width ),
    pen_style( in_bg->pen_style ),
    pen_cap_style( in_bg->pen_cap_style ),
    pen_join_style( in_bg->pen_join_style ),
    is_icon_visible( true ),
    pixmap_icon( QPixmap( ":/windowhandler_icon.png" ) ),
    text( in_text ),
    font( in_font->font ),
    font_color( in_font->font_color ),
    text_alignment( Qt::AlignCenter ),
    is_text_in_center( false ),
    margins( 0 ),
    spacing_icon( 0 ),
    spacing_buttons( 0 ),
    is_min_visible( true ),
    is_max_visible( true ),
    pixmap_min( QPixmap( ":/min.svg" ) ),
    pixmap_max( QPixmap( ":/max.svg" ) ),
    pixmap_close( QPixmap( ":/close.svg" ) ),
    brush_buttons( in_btn->brush ),
    pen_buttons_color( in_btn->pen_color ),
    pen_buttons_width( in_btn->pen_width ),
    pen_buttons_style( in_btn->pen_style ),
    pen_buttons_cap_style( in_btn->pen_cap_style ),
    pen_buttons_join_style( in_btn->pen_join_style ),
    is_lmb_pressed( false ),
    is_cursor_overwritten( false )
{
    this->setMouseTracking( true );

    layPlugin   = new QHBoxLayout;
    layIconAndCaption = new QHBoxLayout;
    layButtons = new QHBoxLayout;

    drwIcon     = new qtrDrawing;
    drwText     = new qtrDrawing;

    btnMin      = new qtrButton;
    btnMax      = new qtrButton;
    btnClose    = new qtrButton;

    drwIcon->setPix( pixmap_icon );

    drwText->setText( text );

    btnMin->setPix( pixmap_min );
    btnMax->setPix( pixmap_max );
    btnClose->setPix( pixmap_close );

    btnMin->setBrush( brush_buttons );
    btnMin->setPenColor( pen_buttons_color );
    btnMin->setPenStyle( pen_buttons_style );
    btnMin->setPenCapStyle( pen_buttons_cap_style );
    btnMin->setPenJoinStyle( pen_buttons_join_style );

    btnMax->setBrush( brush_buttons );
    btnMax->setPenColor( pen_buttons_color );
    btnMax->setPenStyle( pen_buttons_style );
    btnMax->setPenCapStyle( pen_buttons_cap_style );
    btnMax->setPenJoinStyle( pen_buttons_join_style );

    btnClose->setBrush( brush_buttons );
    btnClose->setPenColor( pen_buttons_color );
    btnClose->setPenStyle( pen_buttons_style );
    btnClose->setPenCapStyle( pen_buttons_cap_style );
    btnClose->setPenJoinStyle( pen_buttons_join_style );

    layPlugin->setContentsMargins( margins, margins, margins, margins );
    layPlugin->setSpacing( 0 );

    layIconAndCaption->setContentsMargins( 0, 0, 0, 0 );
    layIconAndCaption->setSpacing( spacing_icon );

    layButtons->setContentsMargins( 0, 0, 0, 0 );
    layButtons->setSpacing( spacing_buttons );

    layIconAndCaption->addWidget( drwIcon );
    layIconAndCaption->addWidget( drwText );

    layButtons->addWidget( btnMin );
    layButtons->addWidget( btnMax );
    layButtons->addWidget( btnClose );

    layPlugin->addLayout( layIconAndCaption );
    layPlugin->addLayout( layButtons );

    drwIcon->setMaximumSize( this->size().height(), this->size().height() );
    drwIcon->setMinimumSize( this->size().height(), this->size().height() );

    btnMin->setMaximumSize( this->size().height(), this->size().height() );
    btnMin->setMinimumSize( this->size().height(), this->size().height() );

    btnMax->setMaximumSize( this->size().height(), this->size().height() );
    btnMax->setMinimumSize( this->size().height(), this->size().height() );

    btnClose->setMaximumSize( this->size().height(), this->size().height() );
    btnClose->setMinimumSize( this->size().height(), this->size().height() );

    this->setLayout( layPlugin );
    this->show();
}

/*!
 * \brief Deconstructor
 *
 * Deletes qtrDrawing drwIcon, drwIcon, qtrButton btnMin, btnMax, btnClose and QHBoxLayout layPlugin, layIconAndCaption and layButtons.
 */

qtrWindowHandler::~qtrWindowHandler()
{
    delete layIconAndCaption;
    delete layButtons;
    delete layPlugin;

    delete drwIcon;
    delete drwText;

    delete btnMin;
    delete btnMax;
    delete btnClose;
}

void qtrWindowHandler::setBrush(QBrush in_brush)
{
    brush = in_brush;
    update();
}

/*!
 * \property qtrWindowHandler::brush
 * \brief a QBrush. This is brush for whole plugin's body. You can pass QColor.
 * \sa brush_buttons
 */

QBrush qtrWindowHandler::getBrush()
{
    return brush;
}

void qtrWindowHandler::setPenColor(QColor in_color)
{
    pen_color = in_color;
    update();
}

/*!
 * \property qtrWindowHandler::pen_color
 * \brief a QColor of QPen. This pen is used to draw border over plugin's body.
 * \sa pen_buttons_color, brush
 */

QColor qtrWindowHandler::getPenColor()
{
    return pen_color;
}

void qtrWindowHandler::setPenWidth(qreal in_qreal)
{
    pen_width = in_qreal;
    update();
}

/*!
 * \property qtrWindowHandler::pen_width
 * \brief width of QPen. This is width of border of whole plugin's body.
 * \sa pen_buttons_width
 */

qreal qtrWindowHandler::getPenWidth()
{
    return pen_width;
}

void qtrWindowHandler::setPenStyle(Qt::PenStyle in_style)
{
    pen_style = in_style;
    update();
}

/*!
 * \property qtrWindowHandler::pen_style
 * \brief a QPen style. This is style of border of whole plugin's body.
 * \sa pen_buttons_style
 */

Qt::PenStyle qtrWindowHandler::getPenStyle()
{
    return pen_style;
}

void qtrWindowHandler::setPenCapStyle(Qt::PenCapStyle in_cap)
{
    pen_cap_style = in_cap;
    update();
}

/*!
 * \property qtrWindowHandler::pen_cap_style
 * \brief a QPen cap style. This is cap style of whole plugin's body.
 * \sa pen_buttons_cap_style
 */

Qt::PenCapStyle qtrWindowHandler::getPenCapStyle()
{
    return pen_cap_style;
}

void qtrWindowHandler::setPenJoinStyle(Qt::PenJoinStyle in_join)
{
    pen_join_style = in_join;
    update();
}

/*!
 * \property qtrWindowHandler::pen_join_style
 * \brief a QPen join style. This is join style of whole plugin's body.
 * \sa pen_buttons_join_style
 */

Qt::PenJoinStyle qtrWindowHandler::getPenJoinStyle()
{
    return pen_join_style;
}

void qtrWindowHandler::setIconVisible(bool in_visible)
{
    is_icon_visible = in_visible;
    drwIcon->setVisible( in_visible );
}

/*!
 * \property qtrWindowHandler::is_icon_visible
 * \brief boolean value. \c false will disable (hide) icon on far left side of qtrWindowHandler plugin, \c true will enable (set to visible).
 * \sa is_min_visible, is_max_visible
 */

bool qtrWindowHandler::isIconVisible()
{
    return is_icon_visible;
}

void qtrWindowHandler::setIconPix(QPixmap in_pix)
{
    drwIcon->setPix( in_pix );
}

/*!
 * \property qtrWindowHandler::pixmap_icon
 * \brief a QPixmap. This is pixmap of far left icon.
 * \sa pixmap_min, pixmap_max, pixmap_close
 */

QPixmap qtrWindowHandler::getIconPix()
{
    return drwIcon->getPix();
}

void qtrWindowHandler::setText(QString in_string)
{
    drwText->setText( in_string );
}

/*!
 * \property qtrWindowHandler::text
 * \brief text of handler's caption.
 * \sa font, font_color, text_alignment
 */

QString qtrWindowHandler::getText()
{
    return drwText->getText();
}

void qtrWindowHandler::setFont(QFont in_font)
{
    drwText->setFont( in_font );
}

/*!
 * \property qtrWindowHandler::font
 * \brief a QFont. This sets font of handler's caption.
 * \sa text, font_color, text
 */

QFont qtrWindowHandler::getFont()
{
    return drwText->getFont();
}

void qtrWindowHandler::setFontColor(QColor in_color)
{
    drwText->setFontColor( in_color );
}

/*!
 * \property qtrWindowHandler::font_color
 * \brief a font color. This sets handler's font's color.
 * \sa text, font, text_alignment
 */

QColor qtrWindowHandler::getFontColor()
{
    return drwText->getFontColor();
}

void qtrWindowHandler::setTextAlignment(Qt::Alignment in_alignment)
{
    drwText->setTextAlignment( in_alignment );
}

/*!
 * \property qtrWindowHandler::text_alignment
 * \brief text alignment of caption's text.
 * \sa text, font, font_color
 */

Qt::Alignment qtrWindowHandler::getTextAlignment()
{
    return drwText->getTextAlignment();
}

void qtrWindowHandler::setTextInCenter(bool in_flag)
{
    is_text_in_center = in_flag;

    if( is_text_in_center ){
        QWidget *temp_widget = 0;
        int     width = 0;

        for (int i = 0; i < layButtons->count(); ++i){
            temp_widget = layButtons->itemAt( i )->widget();
            width += temp_widget->size().width();
        }

        layIconAndCaption->setSpacing( width / 2 );
    } else {
        layIconAndCaption->setSpacing( spacing_icon );
    }
}

/*!
 * \property qtrWindowHandler::is_text_in_center
 * \brief bool
 * \warning This property does not work. Will be replaced in next version.
 */

bool qtrWindowHandler::isTextInCenter()
{
    return is_text_in_center;
}

void qtrWindowHandler::setMargins(int in_margins)
{
    margins = in_margins;
    layPlugin->setContentsMargins( margins, margins, margins, margins );
}

/*!
 * \property qtrWindowHandler::margins
 * \brief margins of layPlugin.
 * \note QHBoxLayout needs four margins value but in this case every margin is equal.
 * \sa spacing_icon, spacing_buttons
 */

int qtrWindowHandler::getMargins()
{
    return margins;
}

void qtrWindowHandler::setSpacingIcon(int in_spacing)
{
    spacing_icon = in_spacing;
    layIconAndCaption->setSpacing( in_spacing );
}

/*!
 * \property qtrWindowHandler::spacing_icon
 * \brief spacing value between drwIcon and drwText.
 * \sa margins, spacing_buttons
 */

int qtrWindowHandler::getSpacingIcon()
{
    return spacing_icon;
}

void qtrWindowHandler::setSpacingButtons(int in_spacing)
{
    spacing_buttons = in_spacing;
    layButtons->setSpacing( in_spacing );
}

/*!
 * \property qtrWindowHandler::spacing_buttons
 * \brief spacing between btnMin, btnMax and btnClose.
 * \sa margins, spacing_icon
 */

int qtrWindowHandler::getSpacingButtons()
{
    return spacing_buttons;
}

void qtrWindowHandler::setMinVisible(bool in_visible)
{
    is_min_visible = in_visible;
    btnMin->setVisible( in_visible );
}

/*!
 * \property qtrWindowHandler::is_min_visible
 * \brief boolean value. \c true means that btnMin will be enadled (visible) and \c false means that btnMin will be disabled (hidden).
 * \sa is_icon_visible, is_max_visible
 */

bool qtrWindowHandler::isMinVisible()
{
    return is_min_visible;
}

void qtrWindowHandler::setMaxVisible(bool in_visible)
{
    is_max_visible = in_visible;
    btnMax->setVisible( in_visible );
}

/*!
 * \property qtrWindowHandler::is_max_visible
 * \brief boolean value. \c true means that btnMax will be enadled (visible) and \c false means that btnMin will be disabled (hidden).
 * \sa is_icon_visible, is_min_visible
 */

bool qtrWindowHandler::isMaxVisible()
{
    return is_max_visible;
}

void qtrWindowHandler::setMinPix(QPixmap in_pix)
{
    btnMin->setPix( in_pix );
}

/*!
 * \property qtrWindowHandler::pixmap_min
 * \brief a QPixmap of btnMin.
 * \sa pixmap_icon, pixmap_max, pixmap_close
 */

QPixmap qtrWindowHandler::getMinPix()
{
    return btnMin->getPix();
}

void qtrWindowHandler::setMaxPix(QPixmap in_pix)
{
    btnMax->setPix( in_pix );
}

/*!
 * \property qtrWindowHandler::pixmap_max
 * \brief a QPixmap of btnMax.
 * \sa pixmap_icon, pixmap_min, pixmap_close
 */

QPixmap qtrWindowHandler::getMaxPix()
{
    return btnMax->getPix();
}

void qtrWindowHandler::setClosePix(QPixmap in_pix)
{
    btnClose->setPix( in_pix );
}

/*!
 * \property qtrWindowHandler::pixmap_close
 * \brief a QPixmap of btnClose.
 * \sa pixmap_icon, pixmap_min, pixmap_max
 */

QPixmap qtrWindowHandler::getClosePix()
{
    return btnClose->getPix();
}

void qtrWindowHandler::setBrushButtons(QBrush in_brush)
{
    brush_buttons = in_brush;
    btnMin->setBrush( in_brush );
    btnMax->setBrush( in_brush );
    btnClose->setBrush( in_brush );
}

/*!
 * \property qtrWindowHandler::brush_buttons
 * \brief a QBrush of buttons. You can pass QColor.
 * \note To change every button's appaerance you need to call every button individually.
 * \sa brush
 */

QBrush qtrWindowHandler::getBrushButtons()
{
    return brush_buttons;
}

void qtrWindowHandler::setPenButtonsColor(QColor in_color)
{
    pen_buttons_color = in_color;
    btnMin->setPenColor( in_color );
    btnMax->setPenColor( in_color );
    btnClose->setPenColor( in_color );
}

/*!
 * \property qtrWindowHandler::pen_buttons_color
 * \brief a QPen color of buttons.
 * \note To change every button's appaerance you need to call every button individually.
 * \sa pen_color
 */

QColor qtrWindowHandler::getPenButtonsColor()
{
    return pen_buttons_cap_style;
}

void qtrWindowHandler::setPenButtonsWidth(qreal in_qreal)
{
    pen_buttons_width = in_qreal;
    btnMin->setPenWidth( in_qreal );
    btnMax->setPenWidth( in_qreal );
    btnClose->setPenWidth( in_qreal );
}

/*!
 * \property qtrWindowHandler::pen_buttons_width
 * \brief a QPen width of buttons.
 * \note To change every button's appaerance you need to call every button individually.
 * \sa pen_width
 */

qreal qtrWindowHandler::getPenButtonsWidth()
{
    return pen_buttons_width;
}

void qtrWindowHandler::setPenButtonsStyle(Qt::PenStyle in_style)
{
    pen_buttons_style = in_style;
    btnMin->setPenStyle( in_style );
    btnMax->setPenStyle( in_style );
    btnClose->setPenStyle( in_style );
}

/*!
 * \property qtrWindowHandler::pen_buttons_style
 * \brief a QPen style of buttons.
 * \note To change every button's appaerance you need to call every button individually.
 * \sa pen_style
 */

Qt::PenStyle qtrWindowHandler::getPenButtonsStyle()
{
    return pen_buttons_style;
}

void qtrWindowHandler::setPenButtonsCapStyle(Qt::PenCapStyle in_cap)
{
    pen_buttons_cap_style = in_cap;
    btnMin->setPenCapStyle( in_cap );
    btnMax->setPenCapStyle( in_cap );
    btnClose->setPenCapStyle( in_cap );
}

/*!
 * \property qtrWindowHandler::pen_buttons_cap_style
 * \brief a QPen cap style of buttons.
 * \note To change every button's appaerance you need to call every button individually.
 * \sa pen_cap_style
 */

Qt::PenCapStyle qtrWindowHandler::getPenButtonsCapStyle()
{
    return pen_buttons_cap_style;
}

void qtrWindowHandler::setPenButtonsJoinStyle(Qt::PenJoinStyle in_join)
{
    pen_buttons_join_style = in_join;
    btnMin->setPenJoinStyle( in_join );
    btnMax->setPenJoinStyle( in_join );
    btnClose->setPenJoinStyle( in_join );
}

/*!
 * \property qtrWindowHandler::pen_buttons_join_style
 * \brief a QPen join style of buttons.
 * \note To change every button's apperance you need to call every button individually.
 * \sa pen_join_style
 */

Qt::PenJoinStyle qtrWindowHandler::getPenButtonsJoinStyle()
{
    return pen_buttons_join_style;
}

/*!
 * \brief This function inserts QWidget to layButtons layout. For example - you can add additional button.
 *
 * For more details visit Qt QLayout documentation.
 *
 * \a in_index is int value that indicates where to insert QWidget.
 *
 * \a in_widget is a pointer to QWidget that will be added to layButtons layout.
 */

void qtrWindowHandler::insertWidget(int in_index, QWidget *in_widget)
{
    in_widget->setMaximumSize( this->size().height(), this->size().height() );
    in_widget->setMinimumSize( this->size().height(), this->size().height() );
    this->layButtons->insertWidget( in_index, in_widget );
}

/*!
 * This event is taking care of moving parent window when left mouse button is holding qtrWindowHandler.
 *
 * s_draging is emited if left mouse button is clicked.
 *
 * \a e is accepted
 */

void qtrWindowHandler::mouseMoveEvent(QMouseEvent *e)
{
    if( e->buttons() & Qt::LeftButton ){
        if( is_lmb_pressed ) {
            //Overide cursor just once - that's why it is in if statement. Same with signal.
            if( !is_cursor_overwritten ){
                QApplication::setOverrideCursor( QCursor( Qt::SizeAllCursor ) );
                is_cursor_overwritten = true;
                emit s_draging();
            }
            //Thanks to drag_pos dragging starts on point where mouse were clicked.
            parentWidget()->move( e->globalPos() - drag_pos );
            e->accept();
        }
    }
}

/*!
    \fn void qtrWindowHandler::s_draging()
    This signal is emited when left mouse button is clicked and mouse move event occurs.
 */

/*!
 * This event check if mouse button is clicked and emits signals if mouse is over drwIcon (s_iconClickedLeft or s_iconClickedRight).
 *
 * \a e is accepted
 */

void qtrWindowHandler::mousePressEvent(QMouseEvent *e)
{
    if( e->button() == Qt::LeftButton && drwIcon->rect().contains( e->pos() ) ){
        emit s_iconClickedLeft();
        e->accept();
    }

    if( e->button() == Qt::RightButton && drwIcon->rect().contains( e->pos() ) ){
        emit s_iconClickedRight();
        e->accept();
    }

    if( e->button() == Qt::LeftButton ){
        drag_pos = e->globalPos() - parentWidget()->frameGeometry().topLeft();
        is_lmb_pressed = true;
        e->accept();
    }
}

/*!
    \fn void qtrWindowHandler::s_iconClickedLeft()
    This signal is emited when left mouse button is clicked on drwIcon.
    \sa s_iconClickedRight()
 */

/*!
    \fn void qtrWindowHandler::s_iconClickedRight()
    This signal is emited when right mouse button is clicked on drwIcon.
    \sa s_iconClickedLeft()
 */

/*!
 * This event check if mouse button is released - if so cursor is restored and flags is_cursor_overwritten and is_lmb_pressed goes \c false.
 *
 * \a e is unused.
 * \sa mousePressEvent()
 */

void qtrWindowHandler::mouseReleaseEvent(QMouseEvent *e)
{
    if( e->button() == Qt::LeftButton ){
        QApplication::restoreOverrideCursor();
        is_cursor_overwritten = false;
        is_lmb_pressed = false;
        e->accept();
    }
}

/*!
 * This event check if mouse pointer enters widget's area. If so s_mouseOver signal is emited.
 *
 * \a e is unused.
 * \sa leaveEvent()
 */

void qtrWindowHandler::enterEvent(QEvent *e)
{
    Q_UNUSED(e)

    emit s_mouseOver();
}

/*!
 * This event check if mouse pointer leavers widget's area. If so s_mouseLeave signal is emited.
 *
 * \a e is unused.
 * \sa enterEvent()
 */

void qtrWindowHandler::leaveEvent(QEvent *e)
{
    Q_UNUSED(e)

    emit s_mouseLeave();
}

/*!
    \fn void qtrWindowHandler::s_mouseOver()
    This signal is emited when mouse pointer hovers above widget's area.

    \sa s_mouseLeave(), enterEvent()
 */

/*!
    \fn void qtrWindowHandler::s_mouseLeave()
    This signal is emited when mouse pointer leaves above widget's area.

    \sa s_mouseOver(), leaveEvent()
 */

/*!
 * This event resize drwIcon, btnMin, btnMax and btnClose to keep proportion of pixmap. Now it's 1:1 proportion and it's not possible to change that.
 *
 * \a e is unused.
 */

void qtrWindowHandler::resizeEvent(QResizeEvent *e)
{
    Q_UNUSED(e)

    //This needs to be changed. It's not working as intendend.
    if( is_text_in_center ){
        QWidget *temp_widget = 0;
        int     width = 0;

        for( int i = 0; i < layButtons->count(); ++i ){
            temp_widget = layButtons->itemAt( i )->widget();
            width += temp_widget->size().width();
        }
        layIconAndCaption->setSpacing( width / 2 );
    }

    drwIcon->setMaximumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
    drwIcon->setMinimumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
    btnMin->setMaximumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
    btnMin->setMinimumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
    btnMax->setMaximumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
    btnMax->setMinimumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
    btnClose->setMaximumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
    btnClose->setMinimumSize( this->size().height() - margins * 2, this->size().height() - margins * 2 );
}

/*!
 * Paint event creates QPainter object, sets brush, pen and then draw. qtrDrawing widget is set as a painting device then draw rect that equals plugin's rect.
 *
 * \a e is unused.
 */

void qtrWindowHandler::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)

    //Paint widget's body (background)
    QPainter painter( this );

    painter.setBrush( brush );

    if( pen_width == 0 )
        painter.setPen( Qt::NoPen );
    else
        painter.setPen( QPen( pen_color, pen_width, pen_style, pen_cap_style, pen_join_style ) );

    painter.drawRect( this->rect() );
}
