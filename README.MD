qtrUi
=====

Content
-------
* About
* qtrUi
    * Plugins
    * Classes
    * Other
* Building and installation
* Contact

About
-----
Qt libraries gives you power to code cross platform GUI applications and makes everything look native. That's the power of Qt.

But what if you want to make user interface that look like you want to and look like this on every platform? Sure you can use style sheets and many other options of customisation that Qt comes with but sometimes even those are lacking of some features, especially when trying to draw, let's say, QLabel on QPainter - I did found that amount of CPU usage in such scenario is way too high (it's probably something with paintEvent). That's why I started this small project - to make it easier to make some cool lookin' UI's.

Also, I'm bored as hell and beside of time killing I want to learn something about my favorite libraries.

Oh, and I know what QML is.

![qtrUi](Doc/html/images/doc_qtrWindowHandler.jpg)

My main goal is to make bunch of classes and Qt Creator plugins of UI parts - just like Qt or any other UI libraries are doing. Every part of my libraries (well, most of them) inherits from Qt QWidget class and relies on Qt functionality. I dont want to make another library of widgets - just a couple of most important so it will be possible to make your UI look special.

Whole project is on GPLv3 license so feel free to take a look into source code and cry because of badly written classes. Also, what's more important, feel free to participate. For more info check out license file.

Have fun.

qtrUi
-----

Classes are in two groups - these you can use with Qt Creator by adding them directly to your forms and these you can create via source code only. First group is called "Plugins" and second simply "Classes". "Classes" group contains every class that expands functionality of those in "Plugins" group (for example - qtrToolTip).

Anything that qtrUi need for working and don't match to those groups mentioned above is listed in "Others".

### Plugins

* **qtrDrawing** - Class qtrDrawing provide QPainter drawing power for the rest of qtrUi plugins.
* **qtrButton** - Class qtrButton provides fully customizable push button widget for Qt projects.
* **qtrCheckBox** - Class qtrCheckBox provides two state check box for Qt projects.
* **qtrWindowHandler** - Class qtrWindowHandler provides customisable window decoration and hadnle bar (this one with icon, window caption and buttons such minimize, maximize and close).

### Classes

* **qtrToolTip** - Class qtrToolTip provides fully visualy customisable tool tip for qtrUi projects.

### Other

* **qtrCore** - qtrCore contains functions and structures for faster workflow with qtrUi.

Samples
-------

Building and installation
-------------------------

You need:

* Qt 5.x.x
* make
* gcc
* bash shell (if you want to use build.sh script)

Of course you can use Visual Studio or whatever but I never worked with such software so everything you will read here basically aims for Linux or Windows with cygwin. For more info about building Qt project visit [www.qt.io/developers/](www.qt.io/developers/).

To build any of qtrUi plugins you can simply open .pro file with your Qt Creator and hit build. Ready for action .so file goes to /path/to/qtrUi/bin - and if you want to make them to work with Qt Creator you need to copy or move those files (files named *plugin.so) to your Qt Creator plugins directory.

Or you can use build.bash script for building and installing all plugins at once. You can find it in root directory of qtrUi project.

To build whole project simply type in your console:

````
./build.sh build_plugins <path_to_qmake>
````

And then...

````
./build.sh install <path_to_plugins>
````

... to copy all .so files to given path.

For more options:

````
./build.sh help
````

After all of that you are ready to create some qtr user interfaces.

Contact
-------

Feel free to mail me: [edwin.jablonski@live.com](edwin.jablonski@live.com).