#include "qtrdrawing.h"
#include "qtrdrawingplugin.h"

#include <QtPlugin>

qtrDrawingPlugin::qtrDrawingPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void qtrDrawingPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool qtrDrawingPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *qtrDrawingPlugin::createWidget(QWidget *parent)
{
    return new qtrDrawing(parent);
}

QString qtrDrawingPlugin::name() const
{
    return QLatin1String("qtrDrawing");
}

QString qtrDrawingPlugin::group() const
{
    return QLatin1String("qtrUi");
}

QIcon qtrDrawingPlugin::icon() const
{
    return QIcon(QLatin1String(":/drawing_icon.png"));
}

QString qtrDrawingPlugin::toolTip() const
{
    return QLatin1String("");
}

QString qtrDrawingPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool qtrDrawingPlugin::isContainer() const
{
    return false;
}

QString qtrDrawingPlugin::domXml() const
{
    return QLatin1String("<widget class=\"qtrDrawing\" name=\"qtrDrawing\">\n</widget>\n");
}

QString qtrDrawingPlugin::includeFile() const
{
    return QLatin1String("");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(qtrdrawingplugin, qtrDrawingPlugin)
#endif // QT_VERSION < 0x050000
