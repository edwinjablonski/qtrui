CONFIG      += plugin release
TARGET      = ../../bin/qtrdrawingplugin
TEMPLATE    = lib

HEADERS     = qtrdrawingplugin.h \
              qtrdrawing.h \
              ../qtrCore/qtrcore.h
SOURCES     = qtrdrawingplugin.cpp \
              qtrdrawing.cpp
RESOURCES   = res/icons.qrc
LIBS        += -L. 

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

