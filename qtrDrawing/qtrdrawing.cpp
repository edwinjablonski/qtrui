/**************************************************************************
** qtrDrawing, QPainter device widget, Qt Creator plugin
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrdrawing.h"

/*!
    \class qtrDrawing
    \inmodule qtrUi
    \brief Class qtrDrawing provide QPainter drawing power for the rest of qtrUi plugins.

    \image doc_qtrDrawing.jpg "qtrDrawing"

    qtrDrawing inherits form QWidget. Most of qtrUi plugins are using this class to draw their elements - it is more
    efficient that way. As a Qt Creator plugin qtrDrawing gives you an option to put QPixmap and play with, for example, geometry of it directly in Qt Creator.
    qtrDrawing can replace QLabel thanks to QPainter's drawText() function - you can change font, text string, color and alignment.

    qtrDrawing can draw pixmap or text but can't draw both of them at the same time. It is possible to draw pixmap with alpha channel and background of any color.

    You can use your own QPainterPath to draw custom shapes.

    For more info about QPainter visit Qt documentation.

    \sa qtrCore, HelloWorld
*/

/*!
 * \brief Constructor
 *
 * Sets all variables to default values.
 *
 * Mouse tracking is set to \c true.
 *
 * The \a parent parameter is sent to the QWidget constructor.
 */

qtrDrawing::qtrDrawing(QWidget *parent) :
    QWidget(parent),
    brush( QColor( 0, 0, 0, 0 ) ),
    pen_color( 0, 0, 0, 0 ),
    pen_width( 0 ),
    pen_style( Qt::SolidLine ),
    pen_cap_style( Qt::SquareCap ),
    pen_join_style( Qt::MiterJoin ),
    opacity( 1 ),
    text( "qtrDrawing" ),
    font( "Arial" ),
    font_color( 0, 0, 0, 255 ),
    text_alignment( Qt::AlignCenter )
{
    this->setMouseTracking( true );
    this->show();
}

/*!
 * \brief Constructor
 *
 * Sets all variables to default values.
 *
 * Mouse tracking is set to \c true.
 *
 * \a parent parameter is sent to the QWidget constructor.
 * \a in_font structure contain font properties.
 * \a in_face structure contain properties of brush and pen.
 * \a in_text this text will appear on button's face.
 *
 * \sa qtrCore
 */

qtrDrawing::qtrDrawing(QString in_text, qtr::Font *in_font, qtr::BrushAndPen *in_face, QWidget *parent) :
    QWidget(parent),
    brush( in_face->brush ),
    pen_color( in_face->pen_color ),
    pen_width( in_face->pen_width ),
    pen_style( in_face->pen_style ),
    pen_cap_style( in_face->pen_cap_style ),
    pen_join_style( in_face->pen_join_style ),
    opacity( 1 ),
    text( in_text ),
    font( in_font->font),
    font_color( in_font->font_color ),
    text_alignment( Qt::AlignCenter )
{
    this->setMouseTracking( true );
    this->show();
}

/*!
 * \brief Deconstructor
 *
 * Nothing. Literally.
 */

qtrDrawing::~qtrDrawing()
{
}

void qtrDrawing::setBrush(QBrush in_brush)
{
    brush = in_brush;
    update();
}

/*!
 * \property qtrDrawing::brush
 * \brief a QBrush qtrDrawing is using for drawing body. You can pass QColor.
 * \sa painter_path, pen_color
 */

QBrush qtrDrawing::getBrush()
{
    return brush;
}

void qtrDrawing::setPenColor(QColor in_color)
{
    pen_color = in_color;
    update();
}

/*!
 * \property qtrDrawing::pen_color
 * \brief a QColor of QPen. This value is used to define border color.
 * \sa pen_width
 */

QColor qtrDrawing::getPenColor()
{
    return pen_color;
}

void qtrDrawing::setPenWidth(qreal in_qreal)
{
    pen_width = in_qreal;
    update();
}

/*!
 * \property qtrDrawing::pen_width
 * \brief width of QPen used to draw border.
 * \sa pen_color
 * \note \c pen_width look better when even value is set.
 */

qreal qtrDrawing::getPenWidth()
{
    return pen_width;
}

void qtrDrawing::setPenStyle(Qt::PenStyle in_style)
{
    pen_style = in_style;
    update();
}

/*!
 * \property qtrDrawing::pen_style
 * \brief a QPen pen style.
 * \sa pen_width, pen_color, pen_cap_style, pen_join_style
 */

Qt::PenStyle qtrDrawing::getPenStyle()
{
    return pen_style;
}

void qtrDrawing::setPenCapStyle(Qt::PenCapStyle in_cap)
{
    pen_cap_style = in_cap;
    update();
}

/*!
 * \property qtrDrawing::pen_cap_style
 * \brief a QPen cap style.
 * \sa pen_width, pen_color, pen_style, pen_join_style
 */

Qt::PenCapStyle qtrDrawing::getPenCapStyle()
{
    return pen_cap_style;
}

void qtrDrawing::setPenJoinStyle(Qt::PenJoinStyle in_join)
{
    pen_join_style = in_join;
    update();
}

/*!
 * \property qtrDrawing::pen_join_style
 * \brief a QPen join style.
 * \sa pen_width, pen_color, pen_style, pen_cap_style
 */

Qt::PenJoinStyle qtrDrawing::getPenJoinStyle()
{
    return pen_join_style;
}

void qtrDrawing::setOpacity(qreal in_opacity)
{
    opacity = in_opacity;
    update();
}

/*!
 * \property qtrDrawing::opacity
 * \brief qreal value of opacity.
 */

qreal qtrDrawing::getOpacity()
{
    return opacity;
}

void qtrDrawing::setPainterPath(QPainterPath in_path)
{
    painter_path = in_path;
    update();
}

/*!
 * \property qtrDrawing::painter_path
 * \brief a QPainterPath. You can pass your own QPainterPath to draw custom shapes. See Qt documentation for more details.
 * \note These function is not avaible from Qt Creator WYSIWYG editor.
 */

QPainterPath qtrDrawing::getPainterPath()
{
    return painter_path;
}

void qtrDrawing::setText(QString in_string)
{
    text = in_string;
    update();
}

/*!
 * \property qtrDrawing::text
 * \brief string of text.
 * \note It is not possible to draw pixmap and text in the same time.
 * \sa font, font_color, text_alignment
 */

QString qtrDrawing::getText()
{
    return text;
}

void qtrDrawing::setFont(QFont in_font)
{
    font = in_font;
    update();
}

/*!
 * \property qtrDrawing::font
 * \brief font.
 * \sa text, font_color, text_alignment
 */

QFont qtrDrawing::getFont()
{
    return font;
}

void qtrDrawing::setFontColor(QColor in_color)
{
    font_color = in_color;
    update();
}

/*!
 * \property qtrDrawing::font_color
 * \brief text color.
 * \sa text, font, text_alignment
 */

QColor qtrDrawing::getFontColor()
{
    return font_color;
}

void qtrDrawing::setTextAlignment(Qt::Alignment in_alignment)
{
    text_alignment = in_alignment;
    update();
}

/*!
 * \property qtrDrawing::text_alignment
 * \brief text alignment.
 * \sa text, font, font_color
 */

Qt::Alignment qtrDrawing::getTextAlignment()
{
    return text_alignment;
}

void qtrDrawing::setPix(QPixmap in_pixmap)
{
    pixmap = in_pixmap;
    update();
}

/*!
 * \property qtrDrawing::pixmap
 * \brief pixmap.
 * \note It is not possible to draw text and pixmap at the same time.
 * \note Given pixmap will be resized to qtrDrawing current size (and then resized with every resize event) without keeping proportions.
 */

QPixmap qtrDrawing::getPix()
{
    return pixmap;
}

/*!
 * \brief By using this function you can pass QPen object.
 *
 * \a in_pen - QPen.
 */

void qtrDrawing::setPen(QPen in_pen)
{
    this->setPenColor( in_pen.color() );
    this->setPenWidth( in_pen.width() );
    this->setPenStyle( in_pen.style() );
    this->setPenCapStyle( in_pen.capStyle() );
    this->setPenJoinStyle( in_pen.joinStyle() );
    update();
}

/*!
 * Paint event creates QPainter object, sets brush, pen and then draw. qtrDrawing widget is set as a painting device. If \c painter_path is empty
 * it will draw this widget's size rectangle, otherwise it will draw passed painter path. Then it checks if \c pixmap equals NULL
 * and if not it will draw pixmap, otherwise it will draw text.
 *
 * This event is updated every time when one of the properties is changed.
 *
 * \a e is unused.
 */

void qtrDrawing::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)

    QPainter painter( this );
    painter.setOpacity( opacity );
    painter.setBrush( brush );

    if( pen_width == 0 )
        painter.setPen( Qt::NoPen );
    else
        painter.setPen( QPen( pen_color, pen_width, pen_style, pen_cap_style, pen_join_style ) );

    if( painter_path.isEmpty() )
        painter.drawRect( this->rect() );
    else
        painter.drawPath( painter_path );

    if( pixmap.isNull() ){
        painter.setFont( font );
        painter.setPen( font_color );
        painter.drawText( this->rect(), text_alignment, text );
    } else {
        painter.drawPixmap( this->rect(), pixmap, pixmap.rect() );
    }
}
