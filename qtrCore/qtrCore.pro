#-------------------------------------------------
#
# Project created by QtCreator 2015-04-12T07:57:49
#
#-------------------------------------------------

QT       -= core gui

TARGET = qtrCore
TEMPLATE = lib
LIBS        += -L.

SOURCES += qtrcore.cpp

HEADERS += qtrcore.h

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}
