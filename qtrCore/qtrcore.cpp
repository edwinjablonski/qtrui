/**************************************************************************
** qtrCore
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#include "qtrcore.h"

/*!
    \page qtrCore
    \inmodule qtrUi
    \title qtrCore
    qtrCore contains functions and structures for faster workflow with qtrUi.

    Structures are in qtr namespace.

    You can use those structures for faster and easier coding of many qtrUi widgets - define fonts and BnP (brush and pen) structures and then pass those
    into widgets constructors.

    \section1 struct BrushAndPen
    This structure holds brush and pen properties:

    \table
    \header
        \li Type
        \li Name
    \row
        \li QBrush
        \li brush
    \row
        \li QColor
        \li pen_color
    \row
        \li qreal
        \li pen_width
    \row
        \li Qt::PenStyle
        \li pen_style
    \row
        \li Qt::PenCapStyle
        \li pen_cap_style
    \row
        \li Qt::PenJoinStyle
        \li pen_join_style
    \endtable

    \section1 struct Font
    This structure holds font properties:

    \table
    \header
        \li Type
        \li Name
    \row
        \li QFont
        \li font
    \row
        \li QColor
        \li color
    \endtable

    \section1 struct Opacity
    This structure holds opacity properties:

    \table
    \header
        \li Type
        \li Name
    \row
        \li qreal
        \li normal
    \row
        \li qreal
        \li hover
    \endtable
*/

/*!
    \page qtr::Opacity
    \inmodule qtrUi
    \title qtr::Opacity

    This structure holds opacity properties:
    \sa qtrCore

    \table
    \header
        \li Type
        \li Name
    \row
        \li qreal
        \li normal
    \row
        \li qreal
        \li hover
    \endtable
*/

/*!
    \page qtr::Font
    \inmodule qtrUi
    \title qtr::Font

    This structure holds font properties:
    \sa qtrCore

    \table
    \header
        \li Type
        \li Name
    \row
        \li QFont
        \li font
    \row
        \li QColor
        \li color
    \endtable

*/

/*!
    \page qtr::BrushAndPen
    \inmodule qtrUi
    \title qtr::BrushAndPen

    This structure holds brush and pen properties:
    \sa qtrCore

    \table
    \header
        \li Type
        \li Name
    \row
        \li QBrush
        \li brush
    \row
        \li QColor
        \li pen_color
    \row
        \li qreal
        \li pen_width
    \row
        \li Qt::PenStyle
        \li pen_style
    \row
        \li Qt::PenCapStyle
        \li pen_cap_style
    \row
        \li Qt::PenJoinStyle
        \li pen_join_style
    \endtable
*/
