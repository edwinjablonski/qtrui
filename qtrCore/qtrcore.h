/**************************************************************************
** qtrCore
** Copyright (C) 2015  Edwin Jabłoński <edwin.jablonski.pl@gmail.com>
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
***************************************************************************/

#ifndef QTRCORE_H
#define QTRCORE_H

#include <QPainter>

namespace qtr
{
    struct BrushAndPen
    {
        QBrush              brush;
        QColor              pen_color;
        qreal               pen_width;
        Qt::PenStyle        pen_style;
        Qt::PenCapStyle     pen_cap_style;
        Qt::PenJoinStyle    pen_join_style;
    };

    struct Font
    {
        QFont               font;
        QColor              font_color;
    };

    struct Opacity
    {
        qreal               normal;
        qreal               hover;
    };
}

#endif // QTRCORE_H
